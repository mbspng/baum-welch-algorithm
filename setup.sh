#!/bin/bash
set -e

echo "Setting up  virtual environment..."
python3 -m venv env
source env/bin/activate
python3 -m pip install --upgrade pip

pip install -r requirements.txt

echo "Building binaries..."
mkdir -p build
mkdir -p bin
make all

echo "Extracting data..."
unzip data.zip
