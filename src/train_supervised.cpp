/**
 * @file train_supervised.cpp
 * Trains HMM with MLE.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include "../incl/utils/string_utils.hpp"
#include "../incl/utils/combinator.hpp"
#include "../incl/utils/filereader.hpp"
#include "../incl/utils/load.hpp"
#include "../incl/utils/progress_display.hpp"
#include "../incl/utils/translator.hpp"
#include "../incl/file2dist.hpp"
#include "../incl/hmm.hpp"
#include "../incl/mle.hpp"
#include <math.h>
#include <stdio.h>
#include <string>
#include <vector>

#define NDEBUG

template<class outer_translator_type, class inner_translator_type>
struct TranslatorCompositin
{
    TranslatorCompositin(outer_translator_type& outer_translator,
                         inner_translator_type& inner_translator)
      : outer_translator(outer_translator), inner_translator(inner_translator)
    {
    }

    template<class T>
    auto translate(const T t)
    {
        std::string ret;
        try {
            auto id_contaier = inner_translator[t];

            for (const auto& id : id_contaier) {
                try {
                    ret += outer_translator[id] + " ";
                } catch (std::invalid_argument&) {
                    if (id == 0)
                        ret += "<bos> ";
                    else if (id == 1)
                        ret += "<eos> ";
                } catch (std::domain_error&) {
                    std::cout << "lookup of " << t
                              << " in outer translator failed";
                }
            }

        } catch (std::domain_error&) {
            std::cout << "lookup of " << t << " in inner translator failed";
        }
        return ret;
    }

    size_t                size() { return inner_translator.size(); }
    outer_translator_type outer_translator;
    inner_translator_type inner_translator;
};

// policy to get the first entry from a vector from a tuple
template<class tuple_type>
struct cartesian_product_access_policy
{
    using value_type = typename tuple_type::first_type::value_type;
    const auto& access(const tuple_type& tuple) { return tuple.first[0]; }
};

struct ArithmeticPolicy_1
{
    template<class frequency_type>
    long double compute_mle(const frequency_type& ngram_frq,
                            const frequency_type& nmogram_frq)
    {
        return log(ngram_frq / static_cast<long double>(nmogram_frq));
    }
};

template<class translator_t>
struct File2DistTranslationPolicy_1
{
    using translator_type = translator_t;
    File2DistTranslationPolicy_1(translator_type& translator)
      : translator(translator)
    {
        if (!(translator.is_reserved_id(0))) {
            translator.reserve_next_id();
        }
        if (!(translator.is_reserved_id(1))) {
            translator.reserve_next_id();
        }
    }

    template<class T>
    auto translate(T t)
    {
        return translator[t];
    }

    size_t translate(BOS bos) { return 0; }

    size_t translate(EOS eos) { return 1; }

    translator_type& translator;
};

struct ExtractionPolicy_1
{
    template<class ngram_type, class ngram_dist_type>
    auto extract_count(const ngram_type&      ngram,
                       const ngram_dist_type& ngram_dist)
    {
        auto ret = ngram_dist.find(ngram);
        if (ret == ngram_dist.end()) {
            std::string ngram_str = aux::to_string_wrapper<ngram_type>(ngram);
            throw std::domain_error(
              std::string("ngram '" + ngram_str + "  ' is not a known key."));
        }
        return ret->second;
    }

    template<class ngram_type, class ngram_dist_type>
    auto extract_ngram(const ngram_type&      ngram,
                       const ngram_dist_type& ngram_dist)
    {
        ngram_type nmogram(ngram.begin(), ngram.end() - 1);
        auto       ret = ngram_dist.find(nmogram);
        if (ret == ngram_dist.end()) {
            std::string nmogram_str =
              aux::to_string_wrapper<ngram_type>(nmogram);
            throw std::domain_error(
              std::string("ngram '" + nmogram_str + "' is not a known key."));
        }
        return ret->first;
    }
};

template<class ngram_type>
bool
validate_for_bos_positions(const ngram_type& ngram, size_t size, size_t bos)
{
    bool observed_bos{ false };
    bool continuous{ true };
    // first check whether the bos marks are all next to each other
    for (const auto& token : ngram) {
        if (observed_bos) {
            if (token != bos) {
                continuous = false;
            }
        }
        if (token == bos) {
            observed_bos = true;
            if (!continuous) return false;
        }
    }
    // next make sure that no bos mark is in the last cell
    if (*(ngram.rbegin()) == bos) return false;
    // the ngram is well formed
    return true;
}

void
usage()
{
    std::cerr
      << "Invalid invocation. CLI for this program is defined in src/train.py."
      << "\n";
    exit(1);
}

int
main(int argc, char** argv)
{
    using namespace aux;

    std::vector<std::string> args(argv, argv + argc);

    if (argc != 3) {
        usage();
        printf("usage: <training data file> <ngram size>\n");
        exit(1);
    }

    std::string filename = argv[1];
    auto size = std::stoi(argv[2]);

    if (size < 2) {
        printf(
          "Smallest possible distribution is a bigram distribution. Choose "
          "ngram size >= 2.\n");
        exit(1);
    }

    Filereader               eval_format(filename);
    std::vector<std::string> eval_tokens;
    unsigned                 eval_line_number = 0;
    for (const auto& line : eval_format) {
        eval_line_number++;
        aux::split(line, eval_tokens);
        if (eval_tokens.size() != 2 && eval_tokens.size() != 0) {
            std::cerr << "incorrect format in file " + filename + " in line " +
                           std::to_string(eval_line_number)+": "+line
                      << "\n";
            exit(1);
        }
    }

    using tokenID_type          = size_t;
    using input_translator_type = Translator<std::string>;
    using ngram_translator_type = Translator<std::vector<tokenID_type>>;
    using translation_policy_type =
      File2DistTranslationPolicy_1<input_translator_type>;

    // translates input tokens to IDs
    input_translator_type              input_translator;
    k_tags                             transition_visitor;
    k_tags_1_word                      emission_visitor;
    k_words                            word_unigram_visitor;
    k_tags                             tag_unigram_visitor;
    File2Dist<translation_policy_type> f2d(input_translator);

    // - build a mapping from input tokens to IDs
    // - assemble the IDs into ngrams
    auto dist_n_tags = f2d.make_distribution_from_file(
      filename, size, transition_visitor, input_translator);

    auto dist_n_minus_one_tags = f2d.make_distribution_from_file(
      filename, size - 1, transition_visitor, input_translator);

    auto dist_n_minus_one_tags_1_word = f2d.make_distribution_from_file(
      filename, size, emission_visitor, input_translator, false);

    auto dist_word_unigrams = f2d.make_distribution_from_file(
      filename, 1, word_unigram_visitor, input_translator, false);

    auto dist_tag_unigrams = f2d.make_distribution_from_file(
      filename, 1, tag_unigram_visitor, input_translator, false);

    size_t bos_id = f2d.get_bos_id();
    size_t eos_id = f2d.get_eos_id();

    // translates vectors of IDs (from the mapping of strigs from the
    // input file to IDs by input_translator) that represent ngrams
    // into a single number that the HMM uses internally as the ID of
    // the state associated with this ngram
    ngram_translator_type hidden_var_translator;
    ngram_translator_type observed_var_translator;

    HiddenMarkovModel<log_semiring, dumb_zero_policy> hmm(
      dist_n_minus_one_tags.size() - 2, dist_word_unigrams.size() - 1);

    // HiddenMarkovModel<log_semiring, dumb_zero_policy> hmm;

    // load nmo distribution into an MLE_Computer to then iterate over
    // the n gram distribution and compute mles for each ngram. The
    // corresponding nmo grams and their counts are looked up
    // automatically.
    MLE_Computer<decltype(dist_n_tags),
                 decltype(dist_n_minus_one_tags),
                 ExtractionPolicy_1,
                 ArithmeticPolicy_1>
      mle_c(dist_n_minus_one_tags);

    ProgressDisplay prg(dist_n_tags.size() +
                          dist_n_minus_one_tags_1_word.size(),
                        "training HMM (supervised)");

    // build the HMM

    // - build a mapping from ngrams of IDs to IDs (which are aliases
    //   the HMM uses internally for the ngrams)
    // - compute MLEs and add the correspondng edges to the HMM

    for (const auto& entry : dist_n_tags) {
        prg.run();

        const auto mle = mle_c.mle(entry.first, entry.second);

        decltype(entry.first) src_state(entry.first.begin(),
                                        entry.first.end() - 1);
        decltype(entry.first) tgt_state(entry.first.begin() + 1,
                                        entry.first.end());

        // if the next to last token is a bos marker and the last one
        // is a normal token then this is an ngram the mle of which
        // needs to become an initial probability
        if (entry.first[size - 2] == bos_id &&
            entry.first[size - 1] != bos_id) {
            hmm.add_initial_probability(hidden_var_translator[tgt_state], mle);
            continue;
        }
        // exclude:  pure <bos> ngrams, pure <eos> ngrams and  <eos>
        // continuations
        if (entry.first[size - 1] == bos_id) continue; // pure <bos>
        if (entry.first[0] == eos_id) continue;        // pure <eos>
        if (entry.first[size - 1] == eos_id) continue; // <eos> continutation

        hmm.add_transition(hidden_var_translator[std::move(src_state)],
                           hidden_var_translator[std::move(tgt_state)],
                           mle);
    }

    for (const auto& entry : dist_n_minus_one_tags_1_word) {

        prg.run();

        if (entry.first[size - 1] == bos_id) continue;

        decltype(entry.first) src_state(entry.first.begin(),
                                        entry.first.end() - 1);
        decltype(entry.first) tgt_state(entry.first.end() - 1,
                                        entry.first.end());

        const auto mle = mle_c.mle(entry.first, entry.second);

        hmm.add_emission(hidden_var_translator[std::move(src_state)],
                         observed_var_translator[std::move(tgt_state)],
                         mle);
    }

    // The HMM works with state IDs from an interval [0, n). The
    // mapping from external IDs to states in the HMM can only work if
    // the nth new ngram added as a new state is the nth state added
    // to the HMM, thus all the ngrams that did not get assigned a
    // state in the HMM, because the ngram did not occure in the
    // training data, have to be assigned an ID after all other ngrams
    // have been assigned an ID and represented as a state. The former
    // are all functionally identical to eachother, as all
    // probabilities associated with them are the same (P = 0),
    // therefore it makes no difference which ngram gets assigned
    // which ID; meaning, scrambling the mapping from IDs to ngram
    // would not make a difference for these unobserved ngrams, unlike
    // for the observed ngrams!
    using CartesianProduct =
      Combinator<decltype(dist_tag_unigrams), cartesian_product_access_policy>;

    CartesianProduct cartesian_product(dist_tag_unigrams, size - 1);

    int i = pow(dist_tag_unigrams.size(), size - 1);

    while (i--) {
        auto c = cartesian_product.make_combination();

        // filter ngrams containing bos in nonsensical places
        // generated by the cartesian product
        if (validate_for_bos_positions(c, size - 1, bos_id)) {
            hidden_var_translator[c];
        }
    }

    dist_n_tags.clear();
    dist_n_minus_one_tags.clear();
    dist_n_minus_one_tags_1_word.clear();
    dist_tag_unigrams.clear();
    dist_word_unigrams.clear();

    hmm.distribute_leftover_probability_mass();

    hmm.sort();

    // hmm.show(TranslatorCompositin<input_translator_type,
    // ngram_translator_type>(
    //              input_translator, hidden_var_translator),
    //          TranslatorCompositin<input_translator_type,
    //          ngram_translator_type>(
    //              input_translator, observed_var_translator));

    hmm.validate();

    hmm.export_model();

    for (size_t i = 0; i < hidden_var_translator.size(); i++) {

        auto     hidden_ngram = hidden_var_translator[i];
        unsigned j            = 0;
        for (const auto& ID : hidden_ngram) {
            j++;
            if (ID == 0) {
                std::cout << "<bos>";
            } else if (ID == 1) {
                std::cout << "<eos>";
            } else {
                std::cout << input_translator[ID];
            }
            if (j < hidden_ngram.size()) std::cout << ' ';
        }
        std::cout << '\n';
    }
    for (size_t i = 0; i < observed_var_translator.size(); i++) {
        for (const auto& ID : observed_var_translator[i]) {
            std::cout << input_translator[ID];
        }
        std::cout << '\n';
    }
}
