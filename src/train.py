#!/usr/bin/env python

"""
MIT License

Copyright (c) 2021 Matthias Bisping

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import click
import os.path
import subprocess


def execute_training_binary(command, *params):

    assert command in {"supervised", "unsupervised"}

    root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    bin_path = os.path.join(root_path, "bin")
    binary = os.path.join(bin_path, f"train_{command}")

    if not os.path.isfile(binary):
        raise RuntimeError(f"Missing binary {binary}")

    command = [*map(str, [binary, *params])]

    subprocess.call(command)


@click.group()
def cli():
    """Trains an HMM, either from scratch (cold start) or from a checkpoint (warm start)."""
    pass


def froward_flattened_supervised_context(ctx):
    params = {**ctx.parent.params, **ctx.params}
    execute_training_binary(ctx.command.name, *params.values())


def froward_flattened_unsupervised_context(ctx):
    params = {**ctx.parent.params, **ctx.params}
    execute_training_binary(ctx.parent.command.name, ctx.command.name, *params.values())


@click.option("-o", "--order", type=int, default=2, help="the order of the model, i.e. the n-gram size")
@click.argument("sequence_file")
@cli.command()
@click.pass_context
def supervised(ctx, sequence_file, order):
    """Trains an HMM via maximum likelihood estimate.

    \b
    SEQUENCE_FILE is a file of blank line separated and newline tokenized LABELED training sequences

    \b
    Example:

        \b
        Viele PIS
        meinen VVFIN
        , $,
        daß KOUS
        Perot NE
        auf APPR
        dem ART
        Capitol NE
        gegen APPR
        eine ART
        Wand NN
        laufen VVINF
        würde VAFIN
        . $.

        \b
        So ADV
        erklärt VVFIN
        etwa ADV
        Edward NE
        Brandon NE
        : $.
    """
    froward_flattened_supervised_context(ctx)


@click.option("--convergence_threshold", "-ct", type=float, default=0.25, help="convergence threshold in percent")
@click.option("--add_emissions", "-ae", default=True, help="adds emissions for newly encountered observations")
@click.argument("sequence_file")
@cli.group()
@click.pass_context
def unsupervised(ctx, sequence_file, convergence_threshold, add_emissions):
    """Trains an HMM via expectation maximization.

    \b
    SEQUENCE_FILE is a file of blank line separated and newline tokenized UNLABELED training sequences

    \b
    Example:

        \b
        Viele
        meinen
        ,
        daß
        Perot
        auf
        dem
        Capitol
        gegen
        eine
        Wand
        laufen
        würde
        .

        \b
        So
        erklärt
        etwa
        Edward
        Brandon
        :

    """
    pass


@click.option("-o", "--order", type=int, default=2, help="the order of the model, i.e. the n-gram size")
@click.argument("tag_file")
@unsupervised.command()
@click.pass_context
def cold(ctx, tag_file, order):
    """Trains a model from scratch.

    TAG_FILE is a file of newline separated hidden variable tags
    """
    froward_flattened_unsupervised_context(ctx)


@click.argument("checkpoint_file")
@unsupervised.command()
@click.pass_context
def warm(ctx, checkpoint_file):
    """Trains a model based on a checkpoint (i.e. an existing model).

    CHECKPOINT_FILE is a file in which a previously trained model is stored.
    """
    froward_flattened_unsupervised_context(ctx)


if __name__ == "__main__":
    cli()
