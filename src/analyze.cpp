/**
 * @file analyze.cpp
 * Builds HMM from a definition file and runs viterbi on a file of observed sequences.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include "../incl/utils/string_utils.hpp"
#include "../incl/utils/filereader.hpp"
#include "../incl/utils/translator.hpp"
#include "../incl/hmm.hpp"
#include <iomanip>
#include <string>
#include <vector>

#define NDEBUG

void
usage()
{
    std::cerr
      << "Invalid invocation. CLI for this program is defined in src/analyze.py."
      << "\n";
    exit(1);
}


void
validate_file(const std::string& filename, unsigned token_count)
{

    aux::Filereader               eval_format(filename);
    std::vector<std::string> eval_tokens;
    unsigned                 eval_line_number = 0;
    for (const auto& line : eval_format) {
        eval_line_number++;
        aux::split(line, eval_tokens);
        if (eval_tokens.size() != token_count && eval_tokens.size() != 0) {
            std::cerr << "incorrect format in file " + filename + " in line " +
                std::to_string(eval_line_number) + ": " + line
                      << "\n";
            exit(1);
        }
    }
}

int
main(int argc, char* argv[])
{
    std::vector<std::string> args(argv, argv + argc);

    if (args.size() != 4) usage();

    auto model_file = args[1];
    auto sequence_file = args[2];
    auto make_dense = args[3] == "True" ? true : false;

    HiddenMarkovModel<log_semiring, dumb_zero_policy> hmm;

    hmm.import_model(model_file);

    if (make_dense) hmm.make_dense();

    aux::Filereader          filereader(model_file);
    std::vector<std::string> tokens;

    auto f_hidden_vars_iter = filereader.begin();

    aux::split(*f_hidden_vars_iter, tokens);

    size_t begin_of_hidden_vars;
    size_t begin_of_observed_vars;

    try {
        begin_of_hidden_vars   = stoul(tokens[0]);
        begin_of_observed_vars = stoul(tokens[1]);
    } catch (...) {
        usage();
    }

    aux::Translator<std::string> hidden_var_translator;
    aux::Translator<std::string> observed_var_translator;

    f_hidden_vars_iter += begin_of_hidden_vars;

    size_t tag_count = begin_of_observed_vars - begin_of_hidden_vars;

    unsigned max_hid_size{ 0 };
    while (tag_count--) {
        auto hid_size = aux::utf8_size(*(f_hidden_vars_iter));
        if (hid_size > max_hid_size) max_hid_size = hid_size;
        hidden_var_translator[*f_hidden_vars_iter];
        ++f_hidden_vars_iter;
    }

    auto f_observed_vars_iter = filereader.begin();

    f_observed_vars_iter += begin_of_observed_vars;

    for (; f_observed_vars_iter != filereader.end(); ++f_observed_vars_iter)
        observed_var_translator[*f_observed_vars_iter];

    filereader.close_file();
    
    filereader.load_file(sequence_file);

    validate_file(sequence_file, 1);

    std::vector<size_t> observed_var_sequence;

    for (const auto& line : filereader) {

        if (line.size() != 0) {

            observed_var_sequence.push_back(observed_var_translator[line]);

        } else {

            auto hidden_var_sequence = hmm.viterbi(observed_var_sequence);

            for (size_t i = 0; i < observed_var_sequence.size(); ++i) {
                std::cout
                  << std::setw(max_hid_size) << std::left
                  << hidden_var_translator[hidden_var_sequence[i]] << ' '
                  << observed_var_translator[observed_var_sequence[i]]
                  << "\n";
            }
            std::cout << "\n";
            observed_var_sequence.clear();
        }
    }

}
