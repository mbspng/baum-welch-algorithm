/**
 * @file train_unsupervised.cpp
 * Trains HMM with Baum Welch algorithm.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#include "../incl/utils/combinator.hpp"
#include "../incl/utils/filereader.hpp"
#include "../incl/utils/load.hpp"
#include "../incl/utils/translator.hpp"
#include "../incl/baum_welch.hpp"
#include "../incl/hmm.hpp"
#include <set>
#include <string>
#include <vector>

#define NDEBUG

template<class outer_translator_type, class inner_translator_type>
struct TranslatorCompositin
{
    TranslatorCompositin(outer_translator_type& outer_translator,
                         inner_translator_type& inner_translator)
    {}

    template<class T>
    auto translate(const T t)
    {
        return t;
    }
};

void
eval_file(const std::string& filename, unsigned token_count)
{

    aux::Filereader          eval_format(filename);
    std::vector<std::string> eval_tokens;
    unsigned                 eval_line_number = 0;
    for (const auto& line : eval_format) {
        eval_line_number++;
        aux::split(line, eval_tokens);
        if (eval_tokens.size() != token_count && eval_tokens.size() != 0) {
            std::cerr << "incorrect format in file " + filename + " in line " +
                           std::to_string(eval_line_number) + ": " + line
                      << "\n";
            exit(1);
        }
    }
}

template<class ngram_type>
bool
validate_for_bos_positions(const ngram_type& ngram, size_t size, size_t bos)
{
    bool observed_bos{ false };
    bool continuous{ true };
    // first check whether teh bso amrks are all nextto each other
    for (const auto& token : ngram) {
        if (observed_bos) {
            if (token != bos) { continuous = false; }
        }
        if (token == bos) {
            observed_bos = true;
            if (!continuous) return false;
        }
    }
    // next make sure that no bos mark is in the last cell
    if (*(ngram.rbegin()) == bos) return false;
    // the ngram is well formed
    return true;
}

void
usage()
{
    std::cerr
      << "Invalid invocation. CLI for this program is defined in src/train.py."
      << "\n";
    exit(1);
}

int
main(int argc, char** argv)
{

    std::vector<std::string> args(argv, argv + argc);

    if (args.size() < 6) usage();

    bool        warm_start{ false };
    bool        max_emissions_defined{ false };
    bool        add_emissions{ true };
    std::string sequence_file{};
    std::string hidden_var_file{};
    std::string checkpoint{};
    size_t      max_emissions{ 0 };
    auto        size{ 0 };
    double      convergence_threshold{ .25 };

    if (args[1] == "warm") {
        warm_start = true;
    } else if (args[1] == "cold") {
        warm_start = false;
    } else
        usage();

    try {

        sequence_file         = args[2];
        add_emissions         = args[3] == "True" ? true : false;
        convergence_threshold = std::stod(args[4]);

        if (!warm_start) {
            if (args.size() != 7) usage();
            hidden_var_file = args[5];
            size            = std::stoi(args[6]);

        } else {
            if (args.size() != 6) usage();
            checkpoint = args[5];
        }
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        usage();
    }

    using namespace aux;

    using tokenID_type          = size_t;
    using ngram_type            = std::vector<tokenID_type>;
    using input_translator_type = Translator<std::string>;
    using ngram_translator_type = Translator<ngram_type>;
    using tokenID_container     = std::set<tokenID_type>;
    using CartesianProduct      = Combinator<tokenID_container>;

    input_translator_type                  input_translator;
    ngram_translator_type                  observed_var_translator;
    std::vector<std::vector<tokenID_type>> training_sequences;

    // if HMM is to be warm_started, extract all IDs from the definition file
    if (warm_start) {

        eval_file(sequence_file, 1);

        Filereader filereader(checkpoint);

        auto f_observed_vars_iter = filereader.begin();

        std::vector<std::string> tokens;

        aux::split(*f_observed_vars_iter, tokens);
        size_t begin_of_hidden_vars;
        size_t begin_of_observed_vars;

        try {
            begin_of_hidden_vars   = stoul(tokens[0]);
            begin_of_observed_vars = stoul(tokens[1]);
        } catch (...) {
            usage();
        }

        f_observed_vars_iter += begin_of_observed_vars;

        // make IDs for all emissions known from the existing HMM
        // before making ID for potential new emissions form teh
        // training data.  thsi way teh IDs for teh emissions between
        // the old and the new HMM will match
        for (; f_observed_vars_iter != filereader.end();
             ++f_observed_vars_iter) {
            observed_var_translator[ngram_type(
              1, input_translator[*f_observed_vars_iter])];
        }

        filereader.close_file();
        filereader.load_file(sequence_file);
        std::vector<size_t> sequence;
        // now make IDs for the so far unseen words
        for (const auto& obs : filereader) {

            if (obs.size() > 0) {
                sequence.push_back(observed_var_translator[ngram_type(
                  1, input_translator[obs])]);
            } else {
                training_sequences.emplace_back(sequence);
                sequence.clear();
            }
        }
        filereader.close_file();

        HiddenMarkovModel<log_semiring, dumb_zero_policy> hmm_init;

        hmm_init.import_model(checkpoint);

        hmm_init.make_dense();

        BaumWelch<HiddenMarkovModel<log_semiring, dumb_zero_policy>> bw;
        HiddenMarkovModel<log_semiring, dumb_zero_policy>            hmm;

        bw.set_add_emissions(add_emissions);

        hmm = std::move(bw.train_model(
          training_sequences, std::move(hmm_init), convergence_threshold));

        hmm.export_model();

        // print the hidden variables from the definition file as no new
        // ones have been added to the HMM
        filereader.close_file();
        filereader.load_file(checkpoint);

        auto f_hidden_vars_iter = filereader.begin();

        f_hidden_vars_iter += begin_of_hidden_vars;

        size_t tag_count = begin_of_observed_vars - begin_of_hidden_vars;

        while (tag_count--) {
            std::cout << *f_hidden_vars_iter << "\n";
            ++f_hidden_vars_iter;
        }

        // if emissions have been added then all tokens from the
        // training file have been added as emissions to the HMM
        if (add_emissions) {

            for (size_t i = 0; i < observed_var_translator.size(); i++) {
                for (const auto& ID : observed_var_translator[i]) {
                    std::cout << input_translator[ID];
                }
                std::cout << '\n';
            }
        }
        // if no emissions have been added then all emissions defined
        // previously are still all emissions defined for the HMM
        else {

            auto f_observed_vars_iter_ =
              filereader.begin(); // new iterator needed becuase assignment not
                                  // defined on purpose, as
                                  // Filereader::output_iterator modifies the
                                  // host object's file member

            f_observed_vars_iter_ += begin_of_observed_vars;

            for (; f_observed_vars_iter_ != filereader.end();
                 ++f_observed_vars_iter_) {
                std::cout << *f_observed_vars_iter_ << "\n";
            }
        }
    }

    // if HMM is to be trained from scratch build all IDs from the data files
    else {

        ngram_translator_type hidden_var_translator;
        unsigned              hidden_var_cnt{ 0 };
        unsigned              observed_var_cnt{ 0 };
        tokenID_container     hidden_vars;
        tokenID_type          bos_id = 0;

        eval_file(hidden_var_file, 1);
        eval_file(sequence_file, 1);

        // reserve 0 for BOS
        input_translator.reserve_next_id();
        hidden_vars.insert(bos_id);

        // count and translate hidden variable symbols
        Filereader filereader(hidden_var_file);
        for (const auto& var : filereader) {
            if (var.size() > 0) { hidden_vars.insert(input_translator[var]); }
        }
        filereader.close_file();
        hidden_var_cnt = hidden_vars.size();

        std::vector<size_t> sequence;
        // count and translate observed variable symbols
        filereader.load_file(sequence_file);
        for (const auto& obs : filereader) {

            if (obs.size() > 0) {
                sequence.push_back(observed_var_translator[ngram_type(
                  1, input_translator[obs])]);
            } else {
                training_sequences.emplace_back(sequence);
                sequence.clear();
            }
        }
        filereader.close_file();
        observed_var_cnt = observed_var_translator.size();

        // generate all possible ngrams of tags
        CartesianProduct cartesian_product(hidden_vars, size - 1);

        size_t i = pow(hidden_var_cnt, size - 1);

        // reset hidden_var_cnt to increment it for every ngram of hidden
        // variables that gets added to hidden_var_translator
        hidden_var_cnt = 0;
        while (i--) {
            auto c = cartesian_product.make_combination();
            // filter ngrams containing bos in nonsensical places
            // generated by the cartesian product
            if (validate_for_bos_positions(c, size - 1, bos_id)) {
                hidden_var_translator[c];
                hidden_var_cnt++;
            }
        }

        BaumWelch<HiddenMarkovModel<log_semiring, dumb_zero_policy>> bw;
        HiddenMarkovModel<log_semiring, dumb_zero_policy>            hmm;

        bw.set_add_emissions(add_emissions);

        if (max_emissions_defined) {
            hmm = std::move(bw.train_model(training_sequences,
                                           hidden_var_cnt,
                                           max_emissions,
                                           convergence_threshold));
        } else {
            hmm = std::move(bw.train_model(training_sequences,
                                           hidden_var_cnt,
                                           observed_var_cnt,
                                           convergence_threshold));
        }

        hmm.export_model();

        for (size_t i = 0; i < hidden_var_translator.size(); i++) {

            auto     hidden_ngram = hidden_var_translator[i];
            unsigned j            = 0;
            for (const auto& ID : hidden_ngram) {
                j++;
                std::cout << input_translator[ID];
                if (j < hidden_ngram.size()) std::cout << ' ';
            }
            std::cout << '\n';
        }

        for (size_t i = 0; i < observed_var_translator.size(); i++) {
            for (const auto& ID : observed_var_translator[i]) {
                std::cout << input_translator[ID];
            }
            std::cout << '\n';
        }
    }
}
