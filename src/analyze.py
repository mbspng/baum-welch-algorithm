#!/usr/bin/env python

"""
MIT License

Copyright (c) 2021 Matthias Bisping

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


import click
import os.path
import subprocess


def execute_training_binary(*params):

    root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    bin_path = os.path.join(root_path, "bin")
    binary = os.path.join(bin_path, "analyze")

    if not os.path.isfile(binary):
        raise RuntimeError(f"Missing binary {binary}")

    command = [*map(str, [binary, *params])]
    print(command)

    subprocess.run(command)


@click.option("--fast", "-f", type=bool, default=True, help="increases model throughput put also memory usage")
@click.argument("sequence_file")
@click.argument("model_file")
@click.command()
@click.pass_context
def analyze(ctx, model_file, sequence_file, fast):
    """Reads and tags sequences from a file.

    \b
    SEQUENCE_FILE is a file of blank line separated and newline tokenized UNLABELED training sequences

    \b
    Example:

        \b
        Viele PIS
        meinen VVFIN
        , $,
        daß KOUS
        Perot NE
        auf APPR
        dem ART
        Capitol NE
        gegen APPR
        eine ART
        Wand NN
        laufen VVINF
        würde VAFIN
        . $.

        \b
        So ADV
        erklärt VVFIN
        etwa ADV
        Edward NE
        Brandon NE
        : $.
    """
    execute_training_binary(*ctx.params.values())


if __name__ == "__main__":
    analyze()
