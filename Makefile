# make all targets
all:
	@cd build; cmake ..; make

# make documentation
documentation:
	@if [ -a doc/documentation.pdf ] ; then mv doc/documentation.pdf . ; fi;
	@cd doc; find . -name '*' -delete
	@if [ -a documentation.pdf ] ; then mv documentation.pdf doc ; fi;
	@doxygen baum_welch.doxygen
	@mv html doc

clean:
	@find . -name '*.o' -delete
	@find . -name '*.obj' -delete
	@find . -name '*.exe' -delete
	@find . -name '*.gch' -delete
	@find . -name '*.out' -delete
	@find . -name '*.gcov' -delete
	@find . -name '*.gcno' -delete
	@find . -name '*~' -delete
	@find . -name '#*' -delete
	@rm -rf build/*
	@rm -rf data/*
	@rm -rf env/*
	@rm -rf bin/*
	@rm -rf doc/*
