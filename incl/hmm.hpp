/**
 * @file hmm.hpp
 * implementation of a Hidden Markov model
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __HMM__
#define __HMM__

#include "utils/string_utils.hpp"
#include "utils/filereader.hpp"
#include "utils/load.hpp"
#include "random_probability.hpp"
#include <assert.h>
#include <cmath>
#include <exception>
#include <limits>
#include <numeric> // accumlate
#include <ostream>
#include <tuple>
#include <vector>

// excpetion to raise if given distribution does not sum to one
class malformed_distribution_error : public std::exception
{
    std::string m_msg;

public:
    malformed_distribution_error(const std::string& msg = "") : m_msg(msg) {}

    virtual const char* what() const throw() { return m_msg.c_str(); }
};

/// helper object required for printing HMM
struct IdentityTranslator
{
    template<class T>
    auto translate(const T t)
    {
        return t;
    }
};

struct dumb_zero_policy
{
    template<class probability_type>
    void deal_with_zero_probability(probability_type& probability)
    {
        probability = log(.0000000000000001);
    }

    template<class probability_type, class token_type>
    void deal_with_unkown_token(probability_type& probability,
                                const token_type& token)
    {
        probability = log(.0000000000000001);
    }
};

struct log_semiring
{
    template<class T, class U>
    static T mul(const T t1, const U t2)
    {
#ifdef arith_debug
        try {
            check_if_normal(t1);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              " multiplication failed with first argument");
        }
        try {
            check_if_normal(t2);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              "  multiplication failed with second argument (" +
              std::to_string(t2) + ")");
        }
        T ret = t1 + t2;
        try {
            check_if_normal(ret);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              " multiplication failed with result of operation");
        }
        return ret;
#else
        return t1 + t2;
#endif
    }

    template<class T, class U>
    static T div(const T t1, const U t2)
    {
#ifdef arith_debug
        try {
            check_if_normal(t1);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(std::string(e.what()) +
                                       " division failed with first argument");
        }
        try {
            check_if_normal(t2);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(std::string(e.what()) +
                                       " division failed with second argument");
        }
        T ret = t1 - t2;
        try {
            check_if_normal(ret);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              " division failed with result of operation");
        }
        return ret;
#else
        return t1 - t2;
#endif
    }

    template<class T, class U>
    static T add(T t1, U t2)
    {
#ifdef arith_debug
        try {
            check_if_normal(t1);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(std::string(e.what()) +
                                       " addition failed with first argument");
        }
        try {
            check_if_normal(t2);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(std::string(e.what()) +
                                       " addition failed with second argument");
        }

        T additive_neutral_element = make_additive_neutral_element<T>();
        if ((t1 == additive_neutral_element) &&
            (t2 == additive_neutral_element)) {
            T ret = additive_neutral_element;
            try {
                check_if_normal(ret);
            } catch (std::underflow_error& e) {
                throw std::underflow_error(
                  std::string(e.what()) +
                  " addition failed with result of operation");
            }
            return ret;
        }

        if (t2 > t1) {
            T tmp = t2;
            t2    = t1;
            t1    = tmp;
        }

        T ret = t1 + log(1 + exp(t2 - t1));
        try {
            check_if_normal(ret);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              " addition failed with result of operation");
        }
        return ret;
#else
        T additive_neutral_element = make_additive_neutral_element<T>();
        if ((t1 == additive_neutral_element) &&
            (t2 == additive_neutral_element))
            return additive_neutral_element;
        if (t2 > t1) {
            T tmp = t2;
            t2    = t1;
            t1    = tmp;
        }
        return t1 + log(1 + exp(t2 - t1));
#endif
    }

    template<class T, class U>
    static T sub(const T t1, const U t2)
    {
#ifdef arith_debug
        try {
            check_if_normal(t1);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              " subtraction failed with first argument");
        }
        try {
            check_if_normal(t2);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              " subtraction failed with second argument");
        }

        T additive_neutral_element = make_additive_neutral_element<T>();
        if ((t1 == additive_neutral_element) &&
            (t2 == additive_neutral_element))
            return additive_neutral_element;
        if (almost_equal(t1, t2)) return additive_neutral_element;
        T ret = log(exp(t1) - exp(t2));
        if (isinf(ret)) return additive_neutral_element;
        try {
            check_if_normal(ret);
        } catch (std::underflow_error& e) {
            throw std::underflow_error(
              std::string(e.what()) +
              " subtraction failed with result of operation with operands " +
              std::to_string(t1) + " and " + std::to_string(t2));
        }
        return ret;
#else
        T additive_neutral_element = make_additive_neutral_element<T>();
        if ((t1 == additive_neutral_element) &&
            (t2 == additive_neutral_element))
            return additive_neutral_element;
        if (almost_equal(t1, t2)) return additive_neutral_element;
        T ret = log(exp(t1) - exp(t2));
        if (std::isinf(ret)) return additive_neutral_element;
        return ret;
#endif
    }

    template<class T>
    static T linear_scale(const T t)
    {
        return exp(t);
    }

    template<class T>
    static auto native_scale(const T t)
    {
        return log(t);
    }

    template<class T>
    constexpr static T make_additive_neutral_element()
    {
        return -1 * std::numeric_limits<T>::infinity();
    }

    template<class T>
    constexpr static T make_multiplicative_neutral_element()
    {
        return static_cast<T>(0);
    }

    // the annihilator of multiplication is the additive neutral element
    template<class T>
    constexpr static T make_annihilator()
    {
        return make_additive_neutral_element<T>();
    }

    template<class T>
    static void check_if_normal(T t)
    {
        T additive_neutral_element = make_additive_neutral_element<T>();
        // probability is allowed to be 0 in logspace
        // probability also is allowed to be -inf in logspace
        if (!std::isnormal(t) && t != 0 && t != additive_neutral_element)
            throw std::underflow_error(
              "Probability of " + std::to_string(t) +
              " is abnormal --- possible cause underflow.");
    }

private:
    template<class T, class U>
    static bool almost_equal(const T     t1,
                             const U     t2,
                             long double threshold = 0.99999)
    {
        T q = std::abs(t2 / t1);
        return threshold <= q && q <= 1 + (1 - threshold);
    }

    template<class T, class U>
    static void check_underflow(const T t1, const U t2)
    {
        if ((t1 < 0.0) == (t2 < 0.0) &&
            std::abs(t2) > std::numeric_limits<double>::max() - std::abs(t1)) {
            throw std::underflow_error("addition of " + std::to_string(t1) +
                                       " and " + std::to_string(t2) +
                                       " will underflow.");
        }
    }
};

// the module realising the transition function of the HMM.  HMM gets
// templated with a "delta engine" aka a class realising the
// transition function.
// Works on sparse vector basis
template<class semiring_type = log_semiring>
class VectorEngine : public semiring_type
{
public: // types
    using transition_type = long double;
    using label_type      = transition_type;
    using stateID_type    = size_t;
    using target_state_transition_pair_type =
      std::pair<stateID_type, transition_type>;
    using inner_edge_container_type =
      std::vector<target_state_transition_pair_type>;
    using source_state_inner_edge_container_pair_type =
      std::pair<stateID_type, inner_edge_container_type>;
    using edge_container_type =
      std::vector<source_state_inner_edge_container_pair_type>;

    using semiring_type::add;
    using semiring_type::sub;
    using semiring_type::mul;
    using semiring_type::div;
    using semiring_type::linear_scale;
    using semiring_type::native_scale;
    using semiring_type::check_if_normal;

public:
    VectorEngine() {}

    VectorEngine(size_t hidden_var_count, size_t observed_var_count)
      : hidden_var_count(hidden_var_count)
      , observed_var_count(observed_var_count)
    {
    }

    size_t get_hidden_var_count() { return hidden_var_count; }
    void set_hidden_var_count(size_t cnt) { hidden_var_count = cnt; }
    size_t get_observed_var_count() { return observed_var_count; }
    void set_observed_var_count(size_t cnt) { observed_var_count = cnt; }

    /// initial probability settor
    /// throws if initial probability not found
    void set_initial_probability(stateID_type    state,
                                 transition_type probability)
    {
        try {
            initial_probability(state) = probability;
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// initial probability gettor
    /// throws if initial probability not found
    auto get_initial_probability(stateID_type state)
    {
        try {
            return initial_probability(state);
        } catch (std::domain_error&) {
            return additive_neutral_element;
        }
    }

    /// transition probability settor
    /// throws if transition not found
    void set_transition_probability(stateID_type    src_state,
                                    stateID_type    tgt_state,
                                    transition_type probability)
    {
        try {
            transition_probability(src_state, tgt_state) = probability;
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// transition probability gettor
    /// throws if transition not found
    auto get_transition_probability(stateID_type src_state,
                                    stateID_type tgt_state)
    {
        try {
            return transition_probability(src_state, tgt_state);
        } catch (std::domain_error&) {
            return additive_neutral_element;
        }
    }

    /// emission probability settor
    /// throws if emission not found
    void set_emission_probability(stateID_type    src_state,
                                  stateID_type    tgt_state,
                                  transition_type probability)
    {
        try {
            emission_probability(src_state, tgt_state) = probability;
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// emission probability gettor
    /// throws if emission not found
    auto get_emission_probability(stateID_type src_state,
                                  stateID_type tgt_state)
    {
        // as emission ID come in from outside the machine, these can lay
        // outside the defined range
        if (tgt_state >= observed_var_count) {
            return additive_neutral_element;
        }
        try {
            return emission_probability(src_state, tgt_state);
        } catch (std::domain_error& e) {
            return additive_neutral_element;
        }
    }

protected:
    /// retrieves initial probability
    /// throws if initial probability not found
    auto& initial_probability(stateID_type state)
    {
        try {
            return find_connected(state, initial_probabilities)->second;
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// retrieves transition probability
    /// throws if transition not found
    auto& transition_probability(stateID_type src_state, stateID_type tgt_state)
    {
        try {
            return find_edge_probability(
              src_state, tgt_state, transition_probabilities);
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// retrieves emission probability
    /// throws if emission not found
    auto& emission_probability(stateID_type src_state, stateID_type tgt_state)
    {
        try {
            return find_edge_probability(
              src_state, tgt_state, emission_probabilities);
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// adds initial probability
    void add_initial_probability(const stateID_type    state,
                                 const transition_type prob)
    {
        if (state >= hidden_var_count) {
            if (state == hidden_var_count) {
                hidden_var_count++;
            } else {
                throw std::runtime_error(
                  "state with ID " + std::to_string(state) +
                  " being added violates invariant of all state IDs being "
                  "in a continuous interval. next permissible ID: " +
                  std::to_string(hidden_var_count));
            }
            // denseness is preserved, thus \b dense does not need
            // to be set to false
        }
        if (dense) {
            auto& tpl  = initial_probabilities[state];
            tpl.second = prob;
            tpl.first  = state;
            return;
        }
        initial_probabilities.push_back(std::make_pair(state, prob));
    }

    /// adds transition to state
    template<class T, class U>
    void add_transition(const T          src_state,
                        const U          tgt_state,
                        const label_type edg_label)
    {
        if (tgt_state >= hidden_var_count) {
            if (tgt_state == hidden_var_count) {
                hidden_var_count++;
            } else if (src_state == hidden_var_count &&
                       tgt_state == src_state + 1) {
                hidden_var_count += 2;

            } else {
                throw std::runtime_error(
                  "state with ID " + std::to_string(tgt_state) +
                  " being added violates invariant of all state IDs being "
                  "in a continuous interval. next permissible ID: " +
                  std::to_string(hidden_var_count));
            }
            // generally it is not guaranteed that adding new states by
            // expanding the range of state IDs preserves denseness
            dense  = false;
            sorted = false;
        }
        create_edge(src_state, tgt_state, edg_label, transition_probabilities);
    }

    /// adds emission to state
    template<class T, class U>
    void add_emission(const T src, const U emission, const label_type edg_label)
    {
        if (emission >= observed_var_count) {
            if (emission == observed_var_count) {
                observed_var_count++;
            } else {
                throw std::runtime_error(
                  "emission with ID " + std::to_string(emission) +
                  " being added violates invariant of all emission IDs being "
                  "in a continuous interval. next permissible ID: " +
                  std::to_string(observed_var_count));
            }
            // generally it is not guaranteed that adding new states by
            // expanding the range of state IDs preserves denseness
            dense  = false;
            sorted = false;
        }
        create_edge(src, emission, edg_label, emission_probabilities);
    }

    /// adds emission to all states
    template<class T>
    void add_emission(const T emission, const label_type edg_label)
    {
        for (stateID_type src = 0; src < hidden_var_count; ++src) {
            add_emission(src, emission, edg_label);
        }
    }

    // renormalises all distributions
    void renormalise()
    {
        _renormalise_single(initial_probabilities);
        _renormalise_nested(transition_probabilities);
        _renormalise_nested(emission_probabilities);
    }

    /// prints a representation of the transition function
    /// parameters @p hidden_var_translator and @p observed_var_translator can
    /// be used to translate internal IDs into human readable external tokens
    template<class hidden_var_translator_type   = IdentityTranslator,
             class observed_var_translator_type = IdentityTranslator>
    void show(
      hidden_var_translator_type&& hidden_var_translator = IdentityTranslator(),
      observed_var_translator_type&& observed_var_translator =
        IdentityTranslator())
    {
        std::cout.precision(10);
        std::cout << "from\tto\twith\n";
        auto total = additive_neutral_element;

        // print initial probabilities
        std::cout << "initial probabilities:"
                  << "\n";
        for (const auto& t : initial_probabilities) {
            std::cout << "π(" << hidden_var_translator.translate(t.first)
                      << " (" << t.first << ")) = " << linear_scale(t.second)
                      << "\n";
            total = add(total, t.second);
        }
        std::cout << "\t\t∑ = " << linear_scale(total) << "\n";

        // print transition probabilities
        std::cout << "transition probabilities:"
                  << "\n";
        for (const auto& src : transition_probabilities) {
            total = additive_neutral_element;
            std::cout << hidden_var_translator.translate(src.first) << " ("
                      << src.first << ")\n";
            for (const auto& tgt : src.second) {
                // if (tgt.second != additive_neutral_element) {
                std::cout << "\t" << hidden_var_translator.translate(tgt.first)
                          << " (" << tgt.first << ")\n"
                          << "\t\t" << linear_scale(tgt.second) << "\n";
                // }
                total = add(total, tgt.second);
            }
            std::cout << "\t\t∑ = " << linear_scale(total) << "\n";
        }

        // // print emission probabilities
        // std::cout << "emission probabilities:"
        //           << "\n";
        // for (const auto& src : emission_probabilities) {
        //     total = additive_neutral_element;
        //     std::cout << hidden_var_translator.translate(src.first) << " ("
        //               << src.first << ")\n";
        //     for (const auto& tgt : src.second) {
        //         std::cout << "\t"
        //                   << observed_var_translator.translate(tgt.first)
        //                   << " (" << tgt.first << ")\n"
        //                   << "\t\t" << linear_scale(tgt.second) << "\n";
        //         total = add(total, tgt.second);
        //     }
        //     std::cout << "\t\t∑ = " << linear_scale(total) << "\n";
        // }
    }

    /// sorts all containers of the engine
    void sort()
    {
        std::sort(transition_probabilities.begin(),
                  transition_probabilities.end(),
                  [](const auto& s1, const auto& s2) -> bool {
                      return s1.first < s2.first;
                  });
        for (auto& src : transition_probabilities) {
            auto& tgt_states = src.second;
            std::sort(tgt_states.begin(),
                      tgt_states.end(),
                      [](const auto& s1, const auto& s2) -> bool {
                          return s1.first < s2.first;
                      });
        }
        std::sort(emission_probabilities.begin(),
                  emission_probabilities.end(),
                  [](const auto& s1, const auto& s2) -> bool {
                      return s1.first < s2.first;
                  });
        for (auto& src : emission_probabilities) {
            auto& tgt_states = src.second;
            std::sort(tgt_states.begin(),
                      tgt_states.end(),
                      [](const auto& s1, const auto& s2) -> bool {
                          return s1.first < s2.first;
                      });
        }
        std::sort(initial_probabilities.begin(),
                  initial_probabilities.end(),
                  [](const auto& s1, const auto& s2) -> bool {
                      return s1.first < s2.first;
                  });
        sorted = true;
    }

    /// resizes all containers of the engine to sizes specified by parameters
    void prime_engine()
    {
        initial_probabilities.resize(hidden_var_count);

        transition_probabilities.resize(hidden_var_count);
        for (auto& tgt_states : transition_probabilities)
            tgt_states.second.resize(hidden_var_count);

        emission_probabilities.resize(hidden_var_count);
        for (auto& tgt_states : emission_probabilities)
            tgt_states.second.resize(observed_var_count);
    }

    /// primes the engine and adds default transitions
    void set_up_engine()
    {

        clear();

        prime_engine();

        dense = true;

        for (size_t i = 0; i < hidden_var_count; ++i) {
            add_initial_probability(i, additive_neutral_element);
        }

        for (size_t i = 0; i < hidden_var_count; ++i) {
            for (size_t j = 0; j < hidden_var_count; ++j) {
                add_transition(i, j, additive_neutral_element);
            }
        }

        for (size_t i = 0; i < hidden_var_count; ++i) {
            for (size_t j = 0; j < observed_var_count; ++j) {
                add_emission(i, j, additive_neutral_element);
            }
        }
    }

    /// randomly seeds the state machine and makes it dense
    void random_seed()
    {
        clear();

        prime_engine();

        dense = true;

        aux::Progressbar prg(hidden_var_count +
                               hidden_var_count * hidden_var_count +
                               hidden_var_count * observed_var_count,
                             "randomizing HMM");

        auto rpd = generate_random_probability_distribution(hidden_var_count);
        for (size_t i = 0; i < hidden_var_count; ++i) {
            add_initial_probability(i, native_scale(rpd[i]));
            prg.run();
        }

        for (size_t i = 0; i < hidden_var_count; ++i) {
            std::random_shuffle(rpd.begin(), rpd.end());
            for (size_t j = 0; j < hidden_var_count; ++j) {
                add_transition(i, j, native_scale(rpd[j]));
                prg.run();
            }
        }

        rpd = generate_random_probability_distribution(observed_var_count);
        for (size_t i = 0; i < hidden_var_count; ++i) {
            std::random_shuffle(rpd.begin(), rpd.end());
            for (size_t j = 0; j < observed_var_count; ++j) {
                add_emission(i, j, native_scale(rpd[j]));
                prg.run();
            }
        }
    }

    void clear()
    {
        initial_probabilities.clear();
        emission_probabilities.clear();
        transition_probabilities.clear();
    }

    // makes state machine dense, leading to faster lookup times but
    // more memory usage
    void make_dense(bool show_progress = false, bool smooth_undefined = false)
    {
        // distribute_leftover_probability_mass();

        sort();

        aux::Progressbar prg(hidden_var_count +
                               hidden_var_count * hidden_var_count +
                               hidden_var_count * observed_var_count,
                             "condensing HMM");

        initial_probabilities = make_dense_single(
          initial_probabilities, hidden_var_count, prg, show_progress);
        _make_dense_nested(transition_probabilities,
                           hidden_var_count,
                           hidden_var_count,
                           prg,
                           show_progress);
        _make_dense_nested(emission_probabilities,
                           hidden_var_count,
                           observed_var_count,
                           prg,
                           show_progress);

        dense = true;

        // certain states might not have been defined a source states at all
        // after making the sttae machine dense these will have all transitions
        // set to 0
        if (smooth_undefined) distribute_leftover_probability_mass();
    }

    /// checks whether probabilities sum to 1
    void validate()
    {
        try {
            _validate_single(initial_probabilities);
        } catch (malformed_distribution_error& e) {
            throw malformed_distribution_error(
              "distribution of initial probabilities is malformed: " +
              std::string(e.what()));
        }
        try {
            _validate_nested(transition_probabilities);
        } catch (malformed_distribution_error& e) {
            throw malformed_distribution_error(
              "distribution of transition probabilities is malformed: " +
              std::string(e.what()));
        }
        try {
            _validate_nested(emission_probabilities);
        } catch (malformed_distribution_error& e) {
            throw malformed_distribution_error(
              "distribution of emission probabilities is malformed: " +
              std::string(e.what()));
        }
    }

    /// certain tags may be selected to be excluded from the set of
    /// hidden variables such as EOS, this leads to holes in the
    /// probability distributions of the MLEs for the ngrams of which
    /// the excluded tag is a continutation. The leftover probability
    /// mass neds to be distributed over the other continutations
    void distribute_leftover_probability_mass()
    {
        _distribute_single(initial_probabilities);
        _distribute_nested(transition_probabilities);
        _distribute_nested(emission_probabilities);
    }

private:
    /// makes emissions or transitions dense (depends on argument passed)
    template<class progress_display_type>
    void _make_dense_nested(edge_container_type&   sources,
                            size_t                 source_count,
                            size_t                 target_count,
                            progress_display_type& prg,
                            bool                   show_progress)
    {
        auto                sources_itr = sources.begin();
        edge_container_type new_sources(source_count);

        // builds all edges for a given source
        auto build_all_from = [&](size_t i) {

            inner_edge_container_type new_targets(target_count);

            for (size_t j = 0; j < target_count; ++j) {

                if (show_progress) prg.run();

                new_targets[j] = target_state_transition_pair_type(
                  j, additive_neutral_element);
            }
            return new_targets;
        };

        // loop over all source IDs and copy exsting defined edges
        // make new edges with 0 probability transitions for all
        // undefined edges
        for (size_t i = 0; i < source_count; ++i) {

            new_sources[i].first = i;

            if (show_progress) prg.run();

            // if source is defined at all go into subroutine to copy
            // defined targets
            if (sources_itr != sources.end() && sources_itr->first == i) {

                new_sources[i].second = make_dense_single(
                  sources_itr->second, target_count, prg, show_progress);

                sources_itr++;

            } else {
                new_sources[i].second = build_all_from(i);
            }
        }
        sources = new_sources;
    }

    /// makes target state container dense
    template<class progress_display_type>
    inner_edge_container_type make_dense_single(
      inner_edge_container_type& targets,
      size_t                     target_count,
      progress_display_type&     prg,
      bool                       show_progress)
    {
        inner_edge_container_type new_targets(target_count);
        auto                      targets_itr = targets.begin();

        // loop over all target IDs and copy exsting defined targtes
        // make new targets with 0 probability for all undefined
        // targets
        for (size_t i = 0; i < target_count; ++i) {

            if (show_progress) prg.run();

            new_targets[i].first = i;

            if (targets_itr != targets.end() && targets_itr->first == i) {

                new_targets[i].second = targets_itr->second;
                targets_itr++;

            } else {
                new_targets[i].second = additive_neutral_element;
            }
        }
        return new_targets;
    }

    void _renormalise_single(inner_edge_container_type& states)
    {
        auto sum = std::accumulate(states.begin(),
                                   states.end(),
                                   additive_neutral_element,
                                   [](transition_type s, auto t) {
                                       return semiring_type::add(s, t.second);
                                   });

        if (sum == multiplicative_neutral_element) return;

        // apply same operation on all vector cells
        // possibly becomes parallel under C++17
        std::transform(
          states.begin(), states.end(), states.begin(), [sum](auto t) {
              t.second = semiring_type::div(t.second, sum);
              return t;
          });
    }

    void _renormalise_nested(edge_container_type& source_states)
    {
        for (auto& state : source_states) _renormalise_single(state.second);
    }

    /// checks whether in @p states probabilities sum to 1
    void _validate_single(const inner_edge_container_type& states)
    {
        transition_type sum{ additive_neutral_element };
        for (const auto& state : states) {
            check_if_normal(state.second);
            sum = add(sum, state.second);
        }

        // If sum differs more than 1/10^10 there might be a logical
        // issue in how the dirstrubution was constructed. This value
        // stems from experimenting with different values and mght not
        // suite all data sets.
        // if sum is equal to 0 then most likely there is not a error
        // but the HMM has been made dense and states that were not defined
        // as sources before at all have been become sources

        if (sum == additive_neutral_element && dense) return;
        check_if_normal(sum);
        if (std::abs(1 - linear_scale(sum)) >= 0.0000000001) {
            throw malformed_distribution_error(
              "sum of probabilities differs by more than 1/10^10 from 1. "
              "sum "
              "= " +
              std::to_string(linear_scale(sum)));
        }
    }

    /// validates emissions or transitions (depends on argument passed)
    void _validate_nested(edge_container_type& source_states)
    {
        for (const auto& state : source_states) {
            try {
                _validate_single(state.second);
            } catch (const malformed_distribution_error& e) {
                throw malformed_distribution_error(
                  "state " + std::to_string(state.first) + ": " +
                  std::string(e.what()));
            }
        }
    }

    /// performs simple uniform redistribution of leftover probability
    /// mass
    void _distribute_single(inner_edge_container_type& states)
    {
        transition_type leftover{ multiplicative_neutral_element };

        for (const auto& state : states) {
            leftover = sub(leftover, state.second);
        }

        transition_type fraction = div(leftover, native_scale(states.size()));

        if (leftover == additive_neutral_element || !std::isnormal(fraction))
            return;

        for (auto& state : states) state.second = add(state.second, fraction);
    }

    void _distribute_nested(edge_container_type& source_states)
    {
        for (auto& state : source_states) _distribute_single(state.second);
    }

    /// finds egde probability
    /// throws if edge not found
    template<class state_id_container_type>
    transition_type& find_edge_probability(
      stateID_type             src_state,
      stateID_type             tgt_state,
      state_id_container_type& states) const
    {
        // look up the connected state
        // if an edge was found then a probability is guarnateed to
        // exist therefore no second try block is needed for the other
        // find_connected()-call
        // if no edge is found throw
        try {
            return find_connected(tgt_state,
                                  find_connected(src_state, states)->second)
              ->second;
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// fowards to find_procedure
    /// @returns an iterator into @p states to the cell that harbours @p
    /// state
    template<class container_type>
    auto find_connected(const stateID_type state, container_type& states) const
    {
        try {
            return find_procedure(state, std::forward<container_type>(states));
        } catch (std::domain_error& e) {
            throw e;
        }
    }

    /// finds @p state_id in @p states
    /// selects optimal search strategy
    /// @returns an iterator into @p states
    template<class state_id_container_type>
    auto find_procedure(stateID_type              state_id,
                        state_id_container_type&& states) const
    {
        auto ret = states.begin();

        if (dense) {
            // if the id of the state is greater pr equal than size of
            // states then there is no entry for this state
            // only situation in which this should happen is the
            // lookup of a word not encountered in training
            if (state_id >= states.size()) return states.end();
            return ret + state_id;
        }

        if (sorted) {
            ret = std::lower_bound(
              states.begin(),
              states.end(),
              state_id,
              [state_id](const auto& t1, const stateID_type id) -> bool {
                  return t1.first < state_id;
              });
            if (ret->first != state_id) ret = states.end();
        }

        else {
            ret = std::find_if(
              states.begin(), states.end(), [state_id](const auto& t) {
                  return t.first == state_id;
              });
        }

        if (ret == states.end()) {
            throw std::domain_error("state with id " +
                                    std::to_string(state_id) + " not found.");
        }

        return ret;
    }

    /// either adds entirly new edge or adds new target to existing source
    void create_edge(const stateID_type   src_state,
                     const stateID_type   tgt_state,
                     const label_type     edg_label,
                     edge_container_type& sources)
    {
        if (src_state >= hidden_var_count) {
            if (src_state == hidden_var_count) {
                hidden_var_count++;
            } else {
                throw std::runtime_error(
                  "state with ID " + std::to_string(src_state) +
                  " being added violates invariant of all state IDs being "
                  "in a continuous interval. next permissible ID: " +
                  std::to_string(hidden_var_count));
            }
            dense  = false;
            sorted = false;
        }
        if (dense) {
            auto& tpl1  = sources[src_state];
            auto& tpl2  = tpl1.second[tgt_state];
            tpl1.first  = src_state;
            tpl2.first  = tgt_state;
            tpl2.second = edg_label;
            return;
        }
        // try to find src_state as a source
        try {
            auto  src_states_itr = find_connected(src_state, sources);
            auto& tgt_states     = src_states_itr->second;
            // try to find tgt_state in target sof src_state
            try {
                auto tgt_states_itr    = find_connected(tgt_state, tgt_states);
                tgt_states_itr->second = edg_label;

            } catch (std::domain_error&) {
                // add source state as a source and make edge to tgt_state
                // if tgt_state was not found
                add_target(src_states_itr, tgt_state, edg_label);
            }

        } catch (std::domain_error&) {
            // add source state as a source and make edge to tgt_state
            // if src_state was not found
            add_edge(src_state, tgt_state, edg_label, sources);
        }
    }

    /// creates entirely new edge, selects fastest method
    template<class container_type>
    void add_edge(const stateID_type src_state,
                  const stateID_type tgt_state,
                  const label_type   edg_label,
                  container_type&    sources)
    {
        target_state_transition_pair_type           tstp(tgt_state, edg_label);
        inner_edge_container_type                   iec(1, std::move(tstp));
        source_state_inner_edge_container_pair_type ssidcp(src_state,
                                                           std::move(iec));
        sources.emplace_back(ssidcp);
    }

    template<class iterator_type>
    void add_target(iterator_type&     src_state_itr,
                    const stateID_type tgt_state,
                    const label_type   edg_label)
    {
        target_state_transition_pair_type tstp(tgt_state, edg_label);

        src_state_itr->second.emplace_back(tstp);
    }

protected: // fields
    size_t              hidden_var_count{ 0 };
    size_t              observed_var_count{ 0 };
    edge_container_type transition_probabilities;
    std::vector<std::pair<stateID_type, transition_type>> initial_probabilities;
    edge_container_type   emission_probabilities;
    bool                  dense{ false };
    bool                  sorted{ false };
    const transition_type additive_neutral_element =
      semiring_type::template make_additive_neutral_element<transition_type>();
    const transition_type multiplicative_neutral_element =
      semiring_type::template make_multiplicative_neutral_element<
        transition_type>();
    const transition_type annihilator =
      semiring_type::template make_annihilator<transition_type>();
};

template<class semiring_t,
         class zero_probability_policy_type,
         template<typename> class automaton_t = VectorEngine>
class HiddenMarkovModel
  : protected automaton_t<semiring_t>
  , public zero_probability_policy_type
{

public:
    using semiring_type  = semiring_t;
    using automaton_type = automaton_t<semiring_type>;
    using zero_probability_policy_type::deal_with_zero_probability;
    using zero_probability_policy_type::deal_with_unkown_token;
    using probability_type = typename automaton_type::transition_type;
    using typename automaton_type::stateID_type;
    using typename automaton_type::transition_type;
    using typename automaton_type::target_state_transition_pair_type;
    using automaton_type::add_transition;
    using automaton_type::add_emission;
    using automaton_type::get_initial_probability;
    using automaton_type::get_transition_probability;
    using automaton_type::get_emission_probability;
    using automaton_type::set_initial_probability;
    using automaton_type::set_transition_probability;
    using automaton_type::set_emission_probability;
    using automaton_type::random_seed;
    using automaton_type::make_dense;
    using automaton_type::validate;
    using automaton_type::renormalise;
    using automaton_type::distribute_leftover_probability_mass;
    using automaton_type::show;
    using automaton_type::add_initial_probability;
    using automaton_type::prime_engine;
    using automaton_type::set_up_engine;
    using automaton_type::dense;
    using automaton_type::sorted;
    using automaton_type::sort;
    using automaton_type::initial_probabilities;
    using automaton_type::emission_probabilities;
    using automaton_type::transition_probabilities;
    using automaton_type::additive_neutral_element;
    using automaton_type::annihilator;
    using automaton_type::multiplicative_neutral_element;
    using automaton_type::hidden_var_count;
    using automaton_type::observed_var_count;
    using automaton_type::get_hidden_var_count;
    using automaton_type::get_observed_var_count;
    using automaton_type::set_hidden_var_count;
    using automaton_type::set_observed_var_count;
    using semiring_type::add;
    using semiring_type::mul;
    using semiring_type::div;
    using semiring_type::linear_scale;
    using semiring_type::native_scale;
    using semiring_type::check_if_normal;
    using trellis_type            = std::vector<std::vector<transition_type>>;
    using backpointer_matrix_type = std::vector<std::vector<int>>;

    using sequence_type = std::vector<stateID_type>;

    /// default constructor
    HiddenMarkovModel() {}

    /// copy constructor
    HiddenMarkovModel(const HiddenMarkovModel& rhs)
    {
        // call copy assignment
        *this = rhs;
    }

    /// move constructor
    HiddenMarkovModel(HiddenMarkovModel&& rhs)
    {
        // call move assignment
        *this = rhs;
    }

    HiddenMarkovModel(size_t hidden_var_count, size_t observed_var_count)
      : automaton_type(hidden_var_count, observed_var_count)
    {
    }

    /// copy assignment operator
    HiddenMarkovModel& operator=(const HiddenMarkovModel& rhs)
    {
        initial_probabilities    = rhs.initial_probabilities;
        transition_probabilities = rhs.transition_probabilities;
        emission_probabilities   = rhs.emission_probabilities;
        hidden_var_count         = rhs.hidden_var_count;
        observed_var_count       = rhs.observed_var_count;
        sorted                   = rhs.sorted;
        dense                    = rhs.dense;

        forward_trellis.clear();
        backward_trellis.clear();
        backpointer_matrix.clear();
        return *this;
    }

    /// move assignment operator
    HiddenMarkovModel& operator=(HiddenMarkovModel&& rhs)
    {
        initial_probabilities    = std::move(rhs.initial_probabilities);
        transition_probabilities = std::move(rhs.transition_probabilities);
        emission_probabilities   = std::move(rhs.emission_probabilities);
        hidden_var_count         = rhs.hidden_var_count;
        observed_var_count       = rhs.observed_var_count;
        sorted                   = rhs.sorted;
        dense                    = rhs.dense;

        forward_trellis.clear();
        backward_trellis.clear();
        backpointer_matrix.clear();
        rhs.forward_trellis.clear();
        rhs.backward_trellis.clear();
        rhs.backpointer_matrix.clear();
        return *this;
    }

    /**
      @brief exports the HMM to a format that can be read to reconstruct it
      @param o stream to send to (default std::cout)
     */
    void export_model(std::ostream& o = std::cout)
    {
        o << size() + 5 << " " << size() + get_hidden_var_count() + 5 << " "
          << '\n';
        o.precision(100);
        o << (dense) << "\n"
          << (sorted) << "\n"
          << hidden_var_count << '\n'
          << observed_var_count << '\n';

        for (const auto& i_t : initial_probabilities) {
            if (i_t.second != additive_neutral_element) {
                o << "i " << i_t.first << " " << i_t.second << '\n';
            }
        }

        for (const auto& t_t1 : transition_probabilities) {
            for (const auto& t_t2 : t_t1.second) {
                if (t_t2.second != additive_neutral_element) {
                    o << "t " << t_t1.first << " " << t_t2.first << " "
                      << t_t2.second << '\n';
                }
            }
        }

        for (const auto& e_t1 : emission_probabilities) {
            for (const auto& e_t2 : e_t1.second) {
                if (e_t2.second != additive_neutral_element) {
                    o << "e " << e_t1.first << " " << e_t2.first << " "
                      << e_t2.second << '\n';
                }
            }
        }
    }

    /// returns total number of defined initial probabilities +
    /// transition probabiloties + emission probabilities
    size_t size()
    {
        size_t total = 0;

        for (const auto& i_t : initial_probabilities) {
            if (i_t.second != additive_neutral_element) {
                total++;
            }
        }

        for (const auto& t_t1 : transition_probabilities) {
            for (const auto& t_t2 : t_t1.second) {
                if (t_t2.second != additive_neutral_element) {
                    total++;
                }
            }
        }

        for (const auto& e_t1 : emission_probabilities) {
            for (const auto& e_t2 : e_t1.second) {
                if (e_t2.second != additive_neutral_element) {
                    total++;
                }
            }
        }

        return total;
    }

    /**
      @brief imports another HMM and add all its transitiosn and emissions to /b
      this
      @param filename file name of file with HMM definition
     */
    void import_model(std::string filename)
    {

        using std::stoul;
        using std::stold;

        aux::Filereader          filereader(filename);
        auto                     f_iter = filereader.begin();
        std::vector<std::string> tokens;

        aux::split(*f_iter, tokens);

        // the lines after the one defined in the first line contain
        // information for building mappings from state IDs and
        // emissions IDs to external tokens
        if (tokens.size() != 2) {
            throw std::runtime_error("file format error in line 1: '" +
                                     *f_iter + "' expected 2 tokens");
        }
        size_t last = stoul(tokens[0]) - 1;

        std::istringstream(*(++f_iter)) >> dense;
        bool tmp_sorted;
        std::istringstream(*(++f_iter)) >> tmp_sorted;
        hidden_var_count   = stoul(*(++f_iter));
        observed_var_count = std::stoul(*(++f_iter));
        if (dense) set_up_engine();

        ++f_iter;

        size_t line_number = 4;
        for (; f_iter != filereader.end(); ++f_iter) {

            line_number++;
            aux::split(*f_iter, tokens);

            if (tokens.size() == 3 && tokens[0] == "i") {

                add_initial_probability(stoul(tokens[1]), stold(tokens[2]));

            } else if (tokens.size() == 4 && tokens[0] == "t") {

                add_transition(
                  stoul(tokens[1]), stoul(tokens[2]), stold(tokens[3]));

            } else if (tokens.size() == 4 && tokens[0] == "e") {

                add_emission(
                  stoul(tokens[1]), stoul(tokens[2]), stold(tokens[3]));

            } else {

                throw std::runtime_error("file format error in line " +
                                         std::to_string(line_number) + ": " +
                                         *f_iter);
            }
            if (line_number == last) return;
        }

        if (tmp_sorted) sort();

        // precision errors stemming for example from the inadequate
        // accuracy of the probabilities in the import file might need
        // to be taken care of by repairing the distributions
        try {
            validate();
        } catch (malformed_distribution_error&) {

            distribute_leftover_probability_mass();
            renormalise();
        }
    }

    /// prints a debug representation of the forward trellis
    template<class hidden_var_translator_type>
    void show_forward_trellis(
      hidden_var_translator_type&& hidden_var_translator)
    {
        show_trellis(
          forward_trellis,
          std::forward<hidden_var_translator_type>(hidden_var_translator));
    }

    /**
      @brief prints a debug representation of the backward trellis
      @param hidden_var_translator class to translate state IDs
    */
    template<class hidden_var_translator_type>
    void show_backward_trellis(
      hidden_var_translator_type&& hidden_var_translator)
    {
        show_trellis(
          backward_trellis,
          std::forward<hidden_var_translator_type>(hidden_var_translator));
    }

    /**
      @brief computes forward probability of @p sequence and fills forward
      trellis
      @param sequence to analyze
    */
    template<class sequence_container_type>
    auto forward(const sequence_container_type& sequence)
    {
        size_t           t{ 0 }, j{ 0 };
        probability_type sum{ additive_neutral_element };
        if (sequence.size() == 0)
            throw std::invalid_argument("empty sequence is invalid");

        initialise_forward_trellis(sequence.size());
        load_sequence(sequence);

        // iterate over forward_trellis to compute sequence likelihood
        while (t < sequence.size()) {
            j = 0;
            while (j < hidden_var_count) {
                forward_trellis[t][j] = forward_probability(t, j);
                j++;
            }
            t++;
        }
        t--;

        // sum over last column
        for (const auto& cell : forward_trellis[t]) sum = add(sum, cell);

        check_if_normal(sum);
        return sum;
    }

    /**
      @brief computes backward probability of @p sequence and fills backward
      trellis
      @param sequence to analyze
    */
    template<class sequence_container_type>
    auto backward(const sequence_container_type& sequence)
    {
        size_t           t{ sequence.size() }, i{ 0 };
        probability_type sum{ additive_neutral_element };
        if (sequence.size() == 0)
            throw std::invalid_argument("empty sequence is invalid");

        initialise_backward_trellis(sequence.size());
        load_sequence(sequence);

        // iterate over backward_trellis to compute sequence likelihood
        while (t--) {
            i = 0;
            while (i < hidden_var_count) {
                ;
                backward_trellis[t][i] = backward_probability(t, i);
                i++;
            }
        }
        t++;

        i = 0;
        // sum over first column
        for (const auto& cell : backward_trellis[0]) {

            auto init_p = get_initial_probability(i);
            if (init_p == annihilator) deal_with_zero_probability(init_p);
            auto emiss_p = get_emission_probability(i, sequence[0]);
            if (emiss_p == annihilator)
                deal_with_unkown_token(emiss_p, sequence[0]);

            sum = add(mul(mul(cell, emiss_p), init_p), sum);
            i++;
        }

        return sum;
    }

    /**
      @brief performs viterbi algorithm on @p sequence
      @param sequence to analyze
    */
    template<class sequence_container_type>
    auto viterbi(const sequence_container_type& sequence)
    {
        // "finding best hidden sequence");
        size_t              t{ 0 }, j{ 0 };
        probability_type    max{ additive_neutral_element };
        std::vector<size_t> hidden_var_sequence(sequence.size());
        if (sequence.size() == 0) return hidden_var_sequence;

        initialise_forward_trellis(sequence.size());
        initialise_backpointer_matrix(sequence.size());
        load_sequence(sequence);

        // iterate over forward_trellis to compute path probabilities
        // and set backpointers
        while (t < sequence.size()) {
            j = 0;
            while (j < hidden_var_count) {
                viterbi_step(t, j++);
            }
            t++;
        }
        t--;

        // argmax over last column
        j = 0;
        for (const auto& cell : forward_trellis[t]) {
            if (cell > max) {
                max                    = cell;
                hidden_var_sequence[t] = j;
            }
            j++;
        }

        while (t--) {
            hidden_var_sequence[t] =
              backpointer_matrix[t + 1][hidden_var_sequence[t + 1]];
        }

        return hidden_var_sequence;
    }

    /// returns field \b sequence
    const sequence_type& get_sequence() { return sequence; }
    /// returns length of field \b sequence
    size_t get_sequence_length() { return sequence.size(); }
    /// returns field \b forward_trellis
    const trellis_type& get_forward_trellis() { return forward_trellis; }
    /// returns field \b backward_trellis
    const trellis_type& get_backward_trellis() { return backward_trellis; }
    /// returns field \b dense
    bool get_dense() { return dense; };
    /// returns field \b sorted
    bool get_sorted() { return sorted; };

    /// debug method
    void validate_trellises()
    {
        auto pf = forward(sequence);
        auto pb = backward(sequence);

        if (std::abs(pf - pb) > 0.0000000001)
            throw std::runtime_error(
              "forward and backward probabilities differ");

        for (int t = 0; t < sequence.size(); t++) {
            probability_type sum{ additive_neutral_element };
            for (int i = 0; i < hidden_var_count; i++) {
                sum =
                  add(sum, mul(forward_trellis[t][i], backward_trellis[t][i]));
            }
            if (std::abs(pf - sum) > 0.0000000001)
                throw std::runtime_error(
                  "sequence probability differs from sum of "
                  "forward and backward probabilities");
        }
    }

    /**
          @brief loads @p container into field sequence
          @param container to load into sequence
        */
    template<class container_type>
    void load_sequence(const container_type& container)
    {
        sequence = sequence_type(container.begin(), container.end());
    }

private:
    /// comptes base case for forward probability
    auto base_case_probability(const size_t j)
    {
        auto init_p = get_initial_probability(j);
        if (init_p == annihilator) deal_with_zero_probability(init_p);
        auto emiss_p = get_emission_probability(j, sequence[0]);
        if (emiss_p == annihilator) deal_with_zero_probability(emiss_p);

        return mul(init_p, emiss_p);
    }

    /// initializes forward trellis
    void initialise_forward_trellis(size_t length)
    {
        initialise_trellis(forward_trellis, length);
    }

    /// initializes backward trellis
    void initialise_backward_trellis(size_t length)
    {
        initialise_trellis(backward_trellis, length);
    }

    /// resets trellis and pimes all coolumsn to the required length
    void initialise_backpointer_matrix(size_t length)
    {
        backpointer_matrix.resize(length);
        for (auto& column : backpointer_matrix) {
            column.clear();
            column.resize(hidden_var_count, -1);
        }
    }

    /// initialises a @p trellis for @p length
    void initialise_trellis(trellis_type& trellis, size_t length)
    {
        trellis.resize(length);
        for (auto& column : trellis) {
            column.clear();
            column.resize(hidden_var_count, additive_neutral_element);
        }
    }

    /// alias for \b forward_probability
    inline auto alpha(const size_t t, const size_t j)
    {
        return forward_probability(t, j);
    }

    /// computes forward probability of cell at forward_trellis[i][j]
    auto forward_probability(const size_t t, const size_t j)
    {
        if (t == 0) {
            return base_case_probability(j);
        } else {

            auto sum = additive_neutral_element;
            int  i   = 0;

            auto emiss_p = get_emission_probability(j, sequence[t]);
            if (emiss_p == annihilator) deal_with_zero_probability(emiss_p);

            for (const auto& cell : forward_trellis[t - 1]) {

                auto trans_p = get_transition_probability(i, j);
                if (trans_p == annihilator) deal_with_zero_probability(trans_p);

                // by invariant the cells in the columns at t-1 cannot
                // contain zero probabilities, as these have been
                // dealt with in the base case, and all other possibly
                // zero probabilities are caught above, thus no check
                // is necessary for whether cell == annihilator

                auto p1 = mul(cell, trans_p);
                check_if_normal(p1);

                auto p2 = mul(p1, emiss_p);
                check_if_normal(p2);

                sum = add(p2, sum);
                i++;
            }
            return sum;
        }
    }

    /// alias for \b backward_probability
    inline auto beta(const size_t t, const size_t j)
    {
        return backward_probability(t, j);
    }

    /// computes backward probability of cell at backward_trellis[i][j]
    auto backward_probability(const size_t t, const size_t i)
    {
        if (t == sequence.size() - 1) {
            return multiplicative_neutral_element;
        }

        else {
            auto sum = additive_neutral_element;
            int  j   = 0;

            for (const auto& cell : backward_trellis[t + 1]) {

                auto emiss_p = get_emission_probability(j, sequence[t + 1]);
                if (emiss_p == annihilator) deal_with_zero_probability(emiss_p);
                auto trans_p = get_transition_probability(i, j);
                if (trans_p == annihilator) deal_with_zero_probability(trans_p);

                // by invariant the cells in the columns at t+1 cannot
                // contain zero probabilities, see comment above

                auto p1 = mul(cell, trans_p);
                check_if_normal(p1);
                auto p2 = mul(p1, emiss_p);
                check_if_normal(p2);

                sum = add(p2, sum);
                j++;
            }
            return sum;
        }
    }

    /// performs viterbi on the cell forward_trellis[t][j]
    void viterbi_step(const size_t t, const size_t j)
    {
        if (t == 0) {
            forward_trellis[t][j] = base_case_probability(j);
        } else {
            probability_type max1 = additive_neutral_element;
            probability_type max2 = additive_neutral_element;
            size_t           i{ 0 };

            auto emiss_p = get_emission_probability(j, sequence[t]);
            if (emiss_p == annihilator) deal_with_zero_probability(emiss_p);

            for (const auto& cell : forward_trellis[t - 1]) {

                auto trans_p = get_transition_probability(i, j);
                if (trans_p == annihilator) deal_with_zero_probability(trans_p);

                // by invariant the cells in the columns at t-1 cannot
                // contain zero probabilities, see comment above

                auto p1 = mul(cell, trans_p);
                check_if_normal(p1);
                if (p1 > max1) {
                    max1                     = p1;
                    backpointer_matrix[t][j] = i;
                }
                auto p2 = mul(p1, emiss_p);
                check_if_normal(p2);
                if (p2 > max2) {
                    max2                  = p2;
                    forward_trellis[t][j] = p2;
                }
                i++;
            }
        }
    }

    /// prints representation of trellis passed as argument
    template<class hidden_var_translator_type>
    void show_trellis(trellis_type&                trellis,
                      hidden_var_translator_type&& hidden_var_translator)
    {
        if (trellis.size() < 1) {
            std::cout << "\n";
        } else {
            if (trellis[0].size() < 1) {
                std::cout << "\n";
            }
        }
        if (backpointer_matrix.size() != trellis.size()) {
            initialise_backpointer_matrix(trellis.size());
        }

        unsigned width  = trellis.size();
        unsigned height = trellis[0].size();
        for (size_t i = 0; i < height; ++i) {
            for (size_t j = 0; j < width; ++j) {
                std::cout << '(' << hidden_var_translator.translate(i) << " ("
                          << i << ") : " << (trellis[j][i]) << ", "
                          << backpointer_matrix[j][i] << ")\t";
            }
            std::cout << "\n";
        }
    }

private:
    trellis_type forward_trellis;  ///< trellis for the forward procedure
    trellis_type backward_trellis; ///< trellis for the backward procedure
    backpointer_matrix_type backpointer_matrix; ///< matrix for the backpointers
                                                /// of the viterbi algorithm
    sequence_type sequence;                     ///< stores sequence to analyze
};

#endif
