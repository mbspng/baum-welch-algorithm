/**
 * @file baum_welch.hpp
 * implementation of the Baum-Welch algorithm
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __BAUM_WELCH__
#define __BAUM_WELCH__

#include "utils/progress_display.hpp"
#include <cmath>
#include <iomanip>
#include <iostream>
struct RandomSeeder {

  template <class hidden_markov_model_type>
  void operator()(hidden_markov_model_type &hmm) const {
    hmm.random_seed();
  }
};

template <class hidden_markov_model_type>
class BaumWelch : public hidden_markov_model_type::semiring_type {
public:
  using probability_type = typename hidden_markov_model_type::probability_type;
  using sequence_type = typename hidden_markov_model_type::sequence_type;
  using trellis_type = typename hidden_markov_model_type::trellis_type;

  using hidden_markov_model_type::semiring_type::add;
  using hidden_markov_model_type::semiring_type::div;
  using hidden_markov_model_type::semiring_type::linear_scale;
  using hidden_markov_model_type::semiring_type::mul;
  using hidden_markov_model_type::semiring_type::native_scale;
  using hidden_markov_model_type::semiring_type::sub;

  void set_add_emissions(bool b) { add_emissions = b; }

  /**
    @brief trains model by starting with an inital model seeded
    by a seeder functor (for example a random seed)
    @param training_sequences the set of training sequences
    @param hidden_var_count number of states for the HMM
    @param observed_var_count number of emissions for the HMM
    @param convergence_threshold threshold in percentage points upon
    reaching of which iteration ought to halt
    @param seeder function object that seeds the initial HMM
  */
  template <class container_type, class seeder_type = RandomSeeder>
  hidden_markov_model_type &
  train_model(const container_type &training_sequences, size_t hidden_var_count,
              size_t observed_var_count, double convergence_threshold = 0.25,
              const seeder_type &seeder = RandomSeeder()) {
    this->hidden_var_count = hidden_var_count;
    this->observed_var_count = observed_var_count;
    hmm.set_hidden_var_count(hidden_var_count);
    hmm.set_observed_var_count(observed_var_count);
    hmm_tmp.set_hidden_var_count(hidden_var_count);
    hmm_tmp.set_observed_var_count(observed_var_count);

    // seed the hmm's initial probability distributions
    seeder(hmm);
    hmm.make_dense();
    hmm.validate();

    _train_model(training_sequences, hidden_var_count, observed_var_count,
                 convergence_threshold);

    return hmm;
  }

  /**
    @brief trains model by starting with an inital model passed
    as argument
    @param training_sequences the set of training sequences
    @param initial_hmm number inital model to train based on
    @param convergence_threshold threshold in percentage points upon
  */
  template <class container_type, class seeder_type = RandomSeeder>
  hidden_markov_model_type &
  train_model(const container_type &training_sequences,
              hidden_markov_model_type initial_hmm,
              double convergence_threshold = 0.25) {
    hmm = std::move(initial_hmm);
    hmm.make_dense();
    hmm.validate();

    this->hidden_var_count = hmm.get_hidden_var_count();
    this->observed_var_count = hmm.get_observed_var_count();
    hmm_tmp.set_hidden_var_count(hidden_var_count);
    hmm_tmp.set_observed_var_count(observed_var_count);

    _train_model(training_sequences, hidden_var_count, observed_var_count,
                 convergence_threshold);

    return hmm;
  }

private:
  /**
    @param training_sequences the set of training sequences
   */
  template <class container_type>
  double compute_data_probability(const container_type &training_sequences) {

    probability_type data_probability = multiplicative_neutral_element;

    int steps_per_update = std::round(training_sequences.size() / 100);
    aux::ProgressDisplay progress(
        100, "computing P(data | λ" + std::to_string(iteration_number) + ")");

    auto step{0};
    for (const auto &training_sequence : training_sequences) {
      if (!(step++ % steps_per_update)) {
        progress.run();
      }
      data_probability = mul(data_probability, hmm.forward(training_sequence));
    }

    return data_probability;
  }

  /**
    @param training_sequences the set of training sequences
    @param hidden_var_count number of states for the HMM
    @param observed_var_count number of emissions for the HMM
    @param convergence_threshold threshold in percentage points upon
    reaching of which iteration ought to halt
   */
  template <class container_type, class seeder_type = RandomSeeder>
  void _train_model(const container_type &training_sequences,
                    size_t hidden_var_count, size_t observed_var_count,
                    double convergence_threshold = 0.25) {

    probability_type data_probability_current = multiplicative_neutral_element;

    auto data_probability_last = compute_data_probability(training_sequences);

    // repeat until convergence
    while (true) {

      ++iteration_number;

      state_expectation = std::vector<probability_type>(
          hidden_var_count, additive_neutral_element);

      state_expectation_ = std::vector<probability_type>(
          hidden_var_count, additive_neutral_element);

      hmm_tmp.set_up_engine();

      expectation_step(training_sequences);

      maximization_step(training_sequences);

      // if more observed variables were observed than specified
      // as the observed_var_count and emissions have not been
      // added for these observations the leftover probability
      // mass needs to be distributed over the emissions
      hmm.distribute_leftover_probability_mass();
      if (add_emissions)
        hmm.renormalise();

      hmm.validate();

      data_probability_current = compute_data_probability(training_sequences);

      probability_type change =
          (1 - (data_probability_current / data_probability_last)) * 100;

      using std::left;
      using std::right;
      using std::setfill;
      using std::setw;

      std::cerr << setw(20) << left
                << "P( data | λ" + std::to_string(iteration_number) + " )"
                << setw(5) << left << "=" << setw(20) << left
                << std::to_string(data_probability_current) + " (native scale)"
                << setw(15) << left << "     change     " << setw(6) << left
                << "=" << setw(9) << left << std::to_string(change) << " %"
                << "\n";

      if (change < convergence_threshold)
        return;

      data_probability_last = data_probability_current;
      data_probability_current = multiplicative_neutral_element;
    }
  }

  /// takes care of preliminary tasks:
  /// - updates sequence related fields
  /// - builds forward and backward trellis
  /// - extends HMMs if observations without corresponding emissions
  ///   occur
  void set_up(const sequence_type &sequence) {
    // if a new observed variable is encountered, add an emission for it
    if (add_emissions) {
      for (const auto &o : sequence) {
        if (o >= observed_var_count) {

          hmm.add_emission(o, additive_neutral_element);
          hmm_tmp.add_emission(o, additive_neutral_element);
          ++observed_var_count;
          make_dense = true;
        }
      }
    }
    // if new emissions have been added make hmm dense again
    if (make_dense) {
      hmm.make_dense();
      hmm_tmp.make_dense();
      make_dense = false;
    }

    load_sequence(sequence);

    probability_of_sequence = hmm.forward(sequence);

    hmm.backward(sequence);
  }

  /// performs expectation step
  /// writes expected counts to \b hmm_tmp
  template <class container_type>
  void expectation_step(const container_type &training_sequences) {

    int steps_per_update = std::round(training_sequences.size() /
                                      100); // update evey 1 percent of progress
    aux::ProgressDisplay progress(
        100, "learning λ" + std::to_string(iteration_number) + " (E-Step)");

    auto step{0};
    for (const auto &training_sequence : training_sequences) {

      if (!(step++ % steps_per_update)) {
        progress.run();
      }

      set_up(training_sequence);

      for (size_t i = 0; i < hidden_var_count; i++) {

        add_expectation_to_state(i);

        add_expectation_to_initial_probability(i);

        for (size_t j = 0; j < hidden_var_count; j++)
          add_expectation_to_transition_probability(i, j);

        for (size_t e = 0; e < observed_var_count; e++)
          add_expectation_to_emission_probability(i, e);
      }
    }
  }

  /// performs maximization step
  /// writes new probabilities to \b hmm
  template <class container_type>
  void maximization_step(const container_type &training_sequences) {

    int steps_per_update = std::round(
        hidden_var_count +
        hidden_var_count * (hidden_var_count + observed_var_count) / 100);
    aux::ProgressDisplay progress(
        steps_per_update,
        "learning λ" + std::to_string(iteration_number) + " (M-Step)");

    probability_type initial_expectation_any{additive_neutral_element};
    auto step{0};
    for (size_t j = 0; j < hidden_var_count; j++) {

      if (!(step++ % steps_per_update)) {
        progress.run();
      }

      initial_expectation_any =
          add(initial_expectation_any, hmm_tmp.get_initial_probability(j));
    }

    for (size_t i = 0; i < hidden_var_count; i++) {

      probability_type expectation_i = state_expectation[i];
      probability_type expectation_i_ = state_expectation_[i];

      maximize_initial_probability(i, initial_expectation_any);

      for (size_t j = 0; j < hidden_var_count; j++) {

        progress.run();

        maximize_transition_probability(i, j, expectation_i_);
      }

      for (size_t e = 0; e < observed_var_count; e++) {

        progress.run();

        maximize_emission_probability(i, e, expectation_i);
      }
    }
  }

  /// maximizes initial probability π_i
  void maximize_initial_probability(const size_t i,
                                    const probability_type expectation_any) {
    hmm.set_initial_probability(
        i, div(hmm_tmp.get_initial_probability(i), expectation_any));
  }

  /// maximizes transition probability a_ij
  void maximize_transition_probability(const size_t i, const size_t j,
                                       const probability_type expectation_i) {
    hmm.set_transition_probability(
        i, j, div(hmm_tmp.get_transition_probability(i, j), expectation_i));
  }

  /// maximizes emission probability b_i(e)
  void maximize_emission_probability(const size_t i, const size_t e,
                                     const probability_type expectation_i) {
    hmm.set_emission_probability(
        i, e, div(hmm_tmp.get_emission_probability(i, e), expectation_i));
  }

  /// adds expectation to state
  void add_expectation_to_state(const size_t i) {
    probability_type expectation = state_expectation[i];
    for (size_t t = 0; t < sequence_length; t++) {
      expectation = add(expectation, gamma(t, i));
    }
    state_expectation[i] = expectation;

    probability_type expectation_ = state_expectation_[i];
    for (size_t t = 0; t < sequence_length - 1; t++) {
      expectation_ = add(expectation_, gamma(t, i));
    }
    state_expectation_[i] = expectation_;
  }

  /// adds expectation to initial probability π_i
  /// note: stores expectation in place where normaly probability goes
  void add_expectation_to_initial_probability(const size_t i) {
    hmm_tmp.set_initial_probability(
        i, add(gamma(0, i), hmm_tmp.get_initial_probability(i)));
  }

  /// adds expectation to transition probability a_ij
  /// note: stores expectation in place where normaly probability goes
  void add_expectation_to_transition_probability(const size_t i,
                                                 const size_t j) {
    probability_type expectation = hmm_tmp.get_transition_probability(i, j);
    for (size_t t = 0; t < sequence_length - 1; t++) {
      expectation = add(expectation, xi(t, i, j));
    }
    hmm_tmp.set_transition_probability(i, j, expectation);
  }

  /// adds expectation to emission probability b_i(e)
  /// note: stores expectation in place where normaly probability goes
  void add_expectation_to_emission_probability(const size_t i, const size_t e) {
    probability_type expectation = hmm_tmp.get_emission_probability(i, e);
    for (size_t t = 0; t < sequence_length; t++) {
      if (sequence[t] == e)
        expectation = add(expectation, gamma(t, i));
    }
    hmm_tmp.set_emission_probability(i, e, expectation);
  }

  //     P(q_t = i, q_t+1 = j, O | λ) = α_t(i) * a_ij * b_j(O_t+1) * β_t+1(j)
  //     P(q_t = i, q_t+1 = j | O, λ)pp = P(q_t = i, q_t+1 = j, O | λ) / P(O |
  //     λ)
  // ==> P(q_t = i, q_t+1 = j | O, λ) = α_t(i) * a_ij * b_j(O_t+1) * β_t+1(j)
  // / P(O | λ)
  const probability_type xi(const size_t t, const size_t i, const size_t j) {

    return div(mul(mul(mul(forward_trellis[t][i],
                           hmm.get_transition_probability(i, j)),
                       hmm.get_emission_probability(j, sequence[t + 1])),
                   backward_trellis[t + 1][j]),
               probability_of_sequence);
  }

  //     P(q_t = i, O | λ) = α_t(i) * β_t+1(j)
  //     P(q_t = i | O, λ) = P(q_t = i, O | λ) / P(O | λ)
  // ==> P(q_t = i | O, λ) = α_t(i) * β_t+1(j) / P(O | λ)
  auto gamma(const size_t t, const size_t i) {
    return div(mul(forward_trellis[t][i], backward_trellis[t][i]),
               probability_of_sequence);
  }

  /// loads @p sequence into field \a sequence
  template <class container_type>
  void load_sequence(const container_type &container) {
    sequence = sequence_type(container.begin(), container.end());
    sequence_length = sequence.size();
  }

private:
  /// this is the HMM being trained
  hidden_markov_model_type hmm;
  /// this HMM is being repurposed to store expectation values
  hidden_markov_model_type hmm_tmp;
  probability_type additive_neutral_element{hmm.additive_neutral_element};
  probability_type multiplicative_neutral_element{
      hmm.multiplicative_neutral_element};
  /// stores hidden_var_count of \b hmm
  size_t hidden_var_count = hmm.get_hidden_var_count();
  /// stores observed_var_count of \b hmm
  size_t observed_var_count = hmm.get_observed_var_count();
  /// stores length of sequence to analyze
  size_t sequence_length;
  /// stores sequence to analyze
  sequence_type sequence;
  /// stores probability sequence to analyze
  probability_type probability_of_sequence;
  /// reference to forward trellis of \b hmm
  const trellis_type &forward_trellis{hmm.get_forward_trellis()};
  /// reference to backward trellis of \b hmm
  const trellis_type &backward_trellis{hmm.get_backward_trellis()};
  /// container for expectation values of single states
  std::vector<probability_type> state_expectation;
  /// container for expectation values of single states that exclude the last
  /// time step every sequence
  std::vector<probability_type> state_expectation_;
  size_t iteration_number{0};
  bool add_emissions{false};
  bool make_dense{false};
};

#endif
