/**
 * @file file2dist.hpp
 * Generates frequency distribution from a file. Behavior is specified
 * via policy classes that are passed to a dedicated distribution helper.
 * class.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __FILE2DIST__
#define __FILE2DIST__

///////////////////////////////////////////////////////////////////////
// every function and object in this file expects input files of the //
// following format:                                                 //
//                                                                   //
// word tag                                                          //
// word tag                                                          //
// word tag                                                          //
//                                                                   //
// word tag                                                          //
// word tag                                                          //
// ...                                                               //
///////////////////////////////////////////////////////////////////////

#include "utils/string_utils.hpp"
#include "utils/busy.hpp"
#include "utils/filereader.hpp"
#include "distribution_helper.hpp"

/// selects the current word, its tag and the tags of the k - 1 previous words
struct k_tags_1_word
{
    template<class sequence_type, class buffer_type>
    void operator()(buffer_type& buffer, const sequence_type& sequence) const
    {
        buffer.pop_back();
        buffer.push_back(sequence[1]);
        buffer.push_back(sequence[0]);
    }
};

/// selects the current tag and the tags of the k - 1 previous words
struct k_tags
{
    template<class sequence_type, class buffer_type>
    void operator()(buffer_type& buffer, const sequence_type& sequence) const
    {
        buffer.push_back(sequence[1]);
    }
};

/// selects the current word and the k - 1 previous words
struct k_words
{
    template<class sequence_type, class buffer_type>
    void operator()(buffer_type& buffer, const sequence_type& sequence) const
    {
        buffer.push_back(sequence[0]);
    }
};

/// special type for EOS marker to guarantee not interfering with the data in
/// the file
struct BOS
{
    friend bool operator<(const BOS& lhs, const BOS& rhs) { return true; }

    friend std::ostream& operator<<(std::ostream& lhs, const BOS& rhs)
    {
        return lhs << "<bos>";
    }

    operator std::string() { return "<bos>"; }
};

/// special type for EOS marker to guarantee not interfering with the data in
/// the file
struct EOS
{
    friend bool operator<(const EOS& lhs, const EOS& rhs) { return true; }
    friend std::ostream& operator<<(std::ostream& lhs, const EOS& rhs)
    {
        return lhs << "<eos>";
    }

    operator std::string() { return "<eos>"; }
};

/// generates frequency distribution from a file
template<class translation_policy>
class File2Dist : public translation_policy
{
public:
    bool add_eos;
    using translation_policy::translate;

    File2Dist(typename translation_policy::translator_type& translator,
              bool                                          add_eos = true)
      : translation_policy(translator), add_eos(add_eos)
    {
    }

    auto get_bos_id() { return translate(BOS()); }

    auto get_eos_id() { return translate(EOS()); }

    template<class visitor_type,
             template<class... types> class storage_type = std::map,
             class key_type                              = size_t>

    storage_type<std::vector<key_type>, size_t> make_distribution_from_file(
      const std::string&                            file_name,
      size_t                                        size,
      const visitor_type&                           visitor,
      typename translation_policy::translator_type& translator,
      bool                                          add_eos = true)
    {

        using namespace aux;
        using disthelper_type = DistributionHelper<key_type, storage_type>;

        BOS bos;
        EOS eos;

        auto                                 bos_id = translate(bos);
        auto                                 eos_id = translate(eos);
        disthelper_type                       dm(size);
        typename disthelper_type::buffer_type buffer_initializer(size, bos_id);
        typename disthelper_type::buffer_type line_buffer(
          2); // 2 because precondition of input format
        Filereader filereader(file_name);

        dm.initialize_buffer(buffer_initializer);

        BusyBlinker              bsy(20000000, "reading " + file_name);
        std::vector<std::string> tokens;
        for (const auto& line : filereader) {
            bsy.run();
            tokens.clear();
            aux::split(line, tokens);
            if (line == "") {
                if (add_eos) {
                    for (size_t i = 0; i < size; i++) {
                        line_buffer.push_back(eos_id);
                        dm(line_buffer, visitor);
                    }
                }

                line_buffer.clear();
                dm.initialize_buffer(buffer_initializer);
                dm.increment(buffer_initializer);
            } else {
                for (const auto& token : tokens) {
                    line_buffer.push_back(translate(token));
                }
                dm(line_buffer, visitor);
            }
        }
        return dm.dump_distribution();
    }
};

#endif
