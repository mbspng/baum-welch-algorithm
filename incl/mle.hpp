/**
 * @file mle.hpp
 * Computes MLEs given a distribution (passed to constructor) for the
 * denominators of MLE fractions. User is intended to pass in the
 * correct numerator for wich the MLE is to be computed as well
 * instanciate the MLE_Computer with a policy to retrieve the correct
 * denominator and a policy that defines the opertion to compute the
 * MLE (depending on algebraic structure, e.g. log-semiring).
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __MLE__
#define __MLE__

#include "utils/string_utils.hpp"
#include <tuple>

template<class ngram_dist_type,
         class nmogram_dist_type,
         class extraction_policy,
         class arithmetic_policy>
struct MLE_Computer
  : extraction_policy
  , arithmetic_policy
{
    using extraction_policy::extract_count;
    using extraction_policy::extract_ngram;
    using arithmetic_policy::compute_mle;
    MLE_Computer(const nmogram_dist_type& nmogram_dist)
      : nmogram_dist(nmogram_dist)
    {
    }

    template<class ngram_type, class frequency_type>
    auto operator()(const ngram_type& ngram, const frequency_type ngram_frq)
    {
        const auto& nmogram     = extract_ngram(ngram, nmogram_dist);
        const auto  nmogram_frq = extract_count(nmogram, nmogram_dist);
        auto        mle         = compute_mle(ngram_frq, nmogram_frq);
        return std::make_tuple(ngram, nmogram, mle);
    }

    template<class ngram_type, class frequency_type>
    auto mle(const ngram_type& ngram, const frequency_type ngram_frq)
    {
        const auto& nmogram     = extract_ngram(ngram, nmogram_dist);
        const auto  nmogram_frq = extract_count(nmogram, nmogram_dist);
        auto        mle         = compute_mle(ngram_frq, nmogram_frq);
        return mle;
    }

    const nmogram_dist_type& nmogram_dist;
};

#endif
