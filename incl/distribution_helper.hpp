/**
 * @file distribution_helper.hpp
 * Produces an associative container from sequences to counts. This
 * container can be ejected (dump_distribution()) afterwards:
 * Extracts entries from a seqence container and stores them in an
 * internal container of sequence containers. The order of extraction
 * depends on a visitor object passed to operator(). The sequences
 * generated from the extracted entries are automatically counted.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __DISTRIBUTION_HELPER__
#define __DISTRIBUTION_HELPER__

#include <boost/circular_buffer.hpp>
#include <map>

template <class key_type, template <class... types> class storage_type>
class DistributionHelper
{
public:
    using size_type     = size_t;
    using sequence_type = std::vector<key_type>;
    using counter_type  = storage_type<sequence_type, size_type>;
    using buffer_type   = boost::circular_buffer<key_type>;

private:
    counter_type counter;
    buffer_type  buffer;

public:
    DistributionHelper(size_type size) : buffer(size) {}

    counter_type dump_distribution() { return std::move(counter); }

    template <class list_type>
    void initialize_buffer(const list_type& list)
    {
        buffer = std::move(buffer_type(list.begin(), list.end()));
    }

    template <class T>
    void increment(const T& t)
    {
        ++counter[sequence_type(t.begin(), t.end())];
    }

    template <class visitor_type>
    void operator()(const buffer_type&  input_buffer,
                    const visitor_type& visitor)
    {
        visitor(buffer, input_buffer);
        ++counter[sequence_type(buffer.begin(), buffer.end())];
    }
};

#endif
