/**
 * @file random_probability.hpp
 * Produces Dirichlet Distribution.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef RANDOM_SPLITTER
#define RANDOM_SPLITTER

#include <iostream>
#include <math.h>
#include <random>
#include <vector>
#include <assert.h>

// Produces Dirichlet Distribution
std::vector<long double>
generate_random_probability_distribution(size_t sample_size)
{
    std::random_device               rd;
    std::mt19937                     gen(rd());
    std::uniform_real_distribution<> dis(1, 0);

    long double              u_sum{ 0 };
    std::vector<long double> U;
    std::vector<long double> P;

    size_t i = sample_size;
    while (i--) {
        auto u_i = -log(dis(gen));
        u_sum += u_i;
        U.push_back(u_i);
    }
    i = sample_size;
    while (i--) {
        assert(U[i] / u_sum != 0);
        P.push_back(U[i] / u_sum);
    }
    return P;
}

#endif
