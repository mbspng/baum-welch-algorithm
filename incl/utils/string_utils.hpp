/**
 * @file color_utils.hpp
 * String related utilities
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __AUX_STRING__HPP
#define __AUX_STRING__HPP

#include "utf8/utf8.h"
#include <sstream>
#include <string>
#include <vector>

namespace aux {

// returns glyph count of utf8 string
unsigned short
utf8_size(std::string s)
{
    return utf8::distance(s.begin(), s.end());
}

/**
 * @brief Converts v of type container_type<T> to a std::string
 * @param v container to convert
 */
template<template<class...> class container_type, typename... Args>
std::string
to_string(const container_type<Args...>& v)
{
    std::string s;
    for (auto t = v.begin(); t != v.end(); ++t) {
        s += std::to_string(*t);
        if (t + 1 != v.end()) s += " ";
    }
    return s;
}

/**
 * @brief splits string \p s1 on all occurances of string \p s2
 */
std::vector<std::string>
split(const std::string& s1, const std::string& s2 = " ")
{
    std::vector<std::string> ret;
    std::string              buffer;
    for (auto s1i = s1.begin(); s1i != s1.end(); s1i++) {
        if (*s1i == *s2.begin() && s1.end() - s1i >= int(s2.size())) {
            bool is_substring = true;
            auto s1i2         = s1i;
            for (auto s2i = s2.begin(); s2i != s2.end(); ++s2i, ++s1i2) {
                if (!(*s2i == *s1i2)) {
                    is_substring = false;
                    break;
                }
            }
            if (is_substring) {
                s1i += s2.size() - 1;
                if (buffer.size() > 0) ret.push_back(buffer);
                buffer = "";
            }
        } else
            buffer += *s1i;
    }
    if (buffer.size() > 0) ret.push_back(buffer);
    return ret;
}

/**
 * @brief splits string \p s1 on all occurances of string \p s2
 */
template<template<class...> class container_type, class sequence_type>
void
split(const sequence_type&           s1,
      container_type<sequence_type>& ret,
      const sequence_type&           s2 = " ")
{
    sequence_type buffer;
    ret.clear();
    for (auto s1i = s1.begin(); s1i != s1.end(); s1i++) {
        if (*s1i == *s2.begin() && s1.end() - s1i >= int(s2.size())) {
            bool is_sequence = true;
            auto s1i2        = s1i;
            for (auto s2i = s2.begin(); s2i != s2.end(); ++s2i, ++s1i2) {
                if (!(*s2i == *s1i2)) {
                    is_sequence = false;
                    break;
                }
            }
            if (is_sequence) {
                s1i += s2.size() - 1;
                if (buffer.size() > 0) ret.push_back(buffer);
                buffer.clear();
            }
        } else
            buffer += *s1i;
    }
    if (buffer.size() > 0) ret.push_back(buffer);
}

template<class container_type>
struct to_string_wrapper
{
    to_string_wrapper(const container_type& container) : container(container) {}

    friend std::ostream& operator<<(std::ostream&            lhs,
                                    const to_string_wrapper& rhs)
    {
        for (const auto& el : rhs.container) {
            lhs << el << ' ';
        }
        return lhs;
    };

    operator std::string() { return to_string(container); }

    const container_type& container;
};
} // aux
#endif
