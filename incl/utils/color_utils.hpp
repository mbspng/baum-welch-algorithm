/**
 * @file color_utils.hpp
 * class for wrapping color codes and easy printing
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __COLOR__HPP
#define __COLOR__HPP

#include <iostream>

namespace auxCOLOR
{

    /// color IDs
    enum ColorID
    {
        DEF = 39,
        RED = 31,
        GREEN = 32,
        BLUE = 34,
        MAGENTA = 35,
        YELLOW = 33,
        CYAN = 36,
        BLACK = 30
    };
///////////////////////////////////////////////////////////////////////
//                                                                   //
//                             ColorString                           //
//                                                                   //
///////////////////////////////////////////////////////////////////////
    /**
     * @brief struct bundling a string, an ANSI color code and an ANSI
     *        bold face code
     * @param s string
     * @param c color ID
     */
    struct ColorString
    {
///////////////////////////////////////////////////////////////////////
        /**
         * @brief constructs a ColorString with color @p c and the
         *        string @p s.
         * @param s string to wrap
         * @param c color to print @p s in
         * @param bold determines whether @p s will be printed bold
         */
        ColorString(std::string s, ColorID c=DEF, bool bold=1)
            :string(s),
             color(c),
             bold(bold)
        {}
///////////////////////////////////////////////////////////////////////
        /// sends \b str in color of \b color to stream @p o
        friend std::ostream& operator<<(std::ostream& o, const ColorString& c)
        {
            return o << c.ansiistring();
        }
///////////////////////////////////////////////////////////////////////
        /// conversion operator to string, returns field \p string
        operator std::string() const { return string; }
///////////////////////////////////////////////////////////////////////
        /// @return size of string field
        int size() { return string.size(); }
///////////////////////////////////////////////////////////////////////
        /// @return concatenates
        friend std::string operator+(ColorString& lhs, std::string&& rhs)
        {
            return lhs.ansiistring()+rhs;
        }

        /// @return concatenates
        friend std::string operator+(std::string&& lhs, ColorString& rhs)
        {
            return lhs+rhs.ansiistring();
        }
///////////////////////////////////////////////////////////////////////
        /// @returns string with anssii-codes
        std::string ansiistring() const
        {
            std::string b;
            bold? b = "1" : b = "0";
            return "\033["+b+";"+color_id_string()+"m"+string+"\033[0m";
        }
///////////////////////////////////////////////////////////////////////
        std::string color_id_string() const
        {
            switch (color)
            {
            case DEF: return "39";
            case RED: return "31";
            case GREEN: return "32";
            case BLUE: return "34";
            case MAGENTA: return "35";
            case YELLOW: return "33";
            case CYAN: return "36";
            case BLACK: return "30";
            }
            return "39";
        }
///////////////////////////////////////////////////////////////////////
        std::string string;  ///< the actual string
        ColorID color; ///< determines the color modifier for the string \p str
        bool bold; ///< determines whether \p str will be modified to bold
///////////////////////////////////////////////////////////////////////
    };
}

#endif // __COLOR__HPP
