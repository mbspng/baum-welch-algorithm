/**
 * @file translator.hpp
 * Translator emulates a bi-directional mapping from external to
 * internal IDs.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef TRANSLATOR_HPP
#define TRANSLATOR_HPP

#include <boost/functional/hash.hpp>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

namespace aux {

template<class EXTERNSYM = std::string>
class Translator
{
public:
    // public types
    typedef EXTERNSYM ES;
    using key_type   = ES;
    using IS         = size_t;
    using value_type = IS;
    typedef std::unordered_map<ES, IS, boost::hash<ES>> ESISM;
    typedef typename ESISM::iterator ESISM_IT;
    typedef std::vector<ESISM_IT>    IDVEC;

    // public methods

    /**
     *  @brief translates an external tokens to an internal ID
     *  @param es the external token to translate into an internal ID
     *  @return the internal ID assigned to the external token
     */
    const IS& ext_to_id(const ES& es)
    {
        auto s = esism.find(es);
        if (s == esism.end()) {
            // make a new entry in esism, with es as the key and the
            // current size of idvec as the value. Also append the
            // returned iterator of esism at the same position in the
            // vector as what has been set as the value, namely
            // (idvec.size()
            const IS is = idvec.size();
            s           = esism.begin();
            s           = esism.insert(s, std::make_pair(es, is));
            idvec.push_back(s);
        }
        return s->second;
    }

    const IS& operator[](const ES& es) { return ext_to_id(es); }

    /**
     *  @brief translates an internal ID to an external token @param
     *  is the internal ID to translate to an external token
     *  @returns the external token associated with the internal ID
     */
    const ES& id_to_ext(const IS& is)
    {
        if (is < idvec.size()) {
            auto ret = idvec[is];
            if (ret == esism.end()) {
                throw std::invalid_argument("ID " + std::to_string(is) +
                                            " is a reserved ID");
            }
            return ret->first;
        }
        throw std::domain_error("unknown ID: " + std::to_string(is));
    }

    const ES& operator[](const IS& is) { return id_to_ext(is); }

    bool is_known_ext(const ES& es) const
    {
        return esism.find(es) != esism.end();
    }

    IS reserve_next_id()
    {
        idvec.push_back(esism.end());
        return idvec.size() - 1;
    }

    bool is_reserved_id(IS is)
    {
        if (is < idvec.size()) {
            return idvec[is] == esism.end();
        }
        return false;
    }

    size_t size() { return esism.size(); }

private:
    // private fields
    ESISM esism; /// maps external tokens to internal IDs
    IDVEC idvec; /// maps internal IDs to external tokens (index |--> iterator)
};
}
#endif
