/**
 * @file load.hpp
 * Class for progress bar functionality
 *
 * compilers:
 *            - clang-700.0.72
 *            - clang 3.5.2 / 3.5.0
 *            - GCC 5.2.0 / 4.8.3
 *            - Microsoft C/C++ 19.00.23026 for x86
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __LOAD__HPP
#define __LOAD__HPP

#ifndef UNIXLIKE
#if defined __linux || defined __APPLE__
#define UNIXLIKE
#endif
#endif

#include "color_utils.hpp"
#include "print_utils.hpp"
#include "string_utils.hpp"
#ifdef UNIXLIKE
#include <unistd.h>
#endif

#include <algorithm>
#include <math.h>
#include <string>

namespace aux {

/**
 * @brief can display a progress bar in the CLI window
 */
class Progressbar
{
#define PSD 8 // size of percentage display

public: //    PUBLIC TYPEDEFS
    typedef auxCOLOR::ColorString CS;
    typedef auxCOLOR::ColorID     CID;
    typedef std::string           sym;
    typedef unsigned long         step;
    typedef std::vector<sym>      symvec;
    typedef std::vector<CS>       csymvec;

public: //     PUBLIC METHODS
    /**
     * @brief initializes Progressbar with the maximum number of steps
     *        \p needed to finish the task the progress of which the
     *        progress bar is supposed to display.
     * @param message message to display while running
     * @param max number of steps after which 100% is to be reached
     * @param barcolor the color of the individual bars
     * @param bar the glyph for the individual bars
     * @param pre the glyph for the progress bar background
     */
    Progressbar(step        max,
                std::string message  = "",
                CID         barcolor = CID::GREEN,
                sym         bar      = "▓",
                sym         pre      = "░")
      : max(max)
      , message(message + " ")
      ,
#ifdef UNIXLIKE
      bar(bar, barcolor)
      , lbracket("")
      , rbracket("")
      , pre(pre)
#else
      bar("#")
      , lbracket("[")
      , rbracket("]")
      , pre("-")
#endif
    {
        current = 0;
        init();
    }

    /// blanks the current line and brings back the cursor
    void cancel(std::ostream& o = std::cerr) const
    {
        blank_line(o);
#ifdef UNIXLIKE
        o << "\e[?25h";
#endif
    }

    /**
     * @brief advances the progress bar every time an interval of x
     *        function calls has been completed
     * @param o the stream to write to
     */
    inline void run(std::ostream& o = std::cerr)
    {
// do not display anything, if stdout is redirected to a file
#ifdef _WIN32
        if (!_isatty(_fileno(stdout))) return;
#else
        if (!isatty(fileno(stdout))) return;
#endif
        if (current % interval == 0 && !(current > max)) {
// do away with the cursor
#ifdef UNIXLIKE
            o << "\e[?25l";
#endif
            // fill line with background symbol
            for (int i = 0; i < PSD + lbracketSize + messageSize; ++i) o << " ";
            for (unsigned i = 0; i < pres; ++i) o << pre;
            o << rbracket << "\r";
            // o.flush();

            // calculate percentage and make a string of it
            float       prct    = ((float)current / max) * 100;
            std::string strprct = std::to_string((int)prct);
            // get percantge to length of three
            if (aux::utf8_size(strprct) < 2) strprct = " " + strprct;
            if (aux::utf8_size(strprct) < 3) strprct = " " + strprct;

            // print percentage and bar
            o << "[" << strprct << "% ] " << message << lbracket;
            for (auto i = pbar.begin(); i != pbar.end(); ++i) o << *i;

            o << "\r";
            // o.flush();

            pbar.push_back(bar);
        }
        if (current >= max - 1) {
            cancel();
        }
        current++;
    }

private: //    PRIVATE METHODS
    /// initialises fields
    void init()
    {
        if (max == 0) {
            throw std::domain_error("invalid range (0,0]");
        }
        barsize      = aux::utf8_size(bar);
        lbracketSize = aux::utf8_size(lbracket);
        rbracketSize = aux::utf8_size(rbracket);
        messageSize  = aux::utf8_size(message);

        cls                = aux::get_terminal_columns();
        if (cls == -1) cls = 30;
        cls -= (PSD + lbracketSize + rbracketSize +
                messageSize); // columns - "DDD% []"

        // it requires updates updates to fill the line with bars of
        // size barsize
        updates = cls / barsize;
        // same goes for pre
        pres = cls / aux::utf8_size(pre);

        // if max is smaller than updates, update as many times as
        // possible, by setting updates = max
        // then concatenate bar f times with itself, so the full
        // line can still be filled despite the lower update count
        if (max <= updates) {
            updates = max;
            int f   = cls / (barsize * updates);

#ifdef _WIN32
            std::string temp_bar = bar;
            for (int i = 0; i < f - 1; ++i) bar += temp_bar;
#else
            std::string temp_bar = bar.string;
            for (int i = 0; i < f - 1; ++i) bar.string += temp_bar;
#endif
        }

        pbar.reserve(updates);

        // Recalculate max, so it is an integer multiple of updates.
        // This way an interval can be an integer and updates*interval
        // be exactly equal to max.
        // This introduces a slight inaccuracy of the progress bar and
        // causes it to reach 100% a little too early, but assures
        // overall consistency regardless of the initial size of max.
        // The amount of time 100% is reached early is strictly less
        // than "update" steps, as the discrepancy between max and an
        // integer multiple of updates closest to max had to be less
        // than the value of updates, otherwise max would have been an
        // exact multiple of updates.
        // The delay therefore has a fix maximum.
        max = floor((double)max / updates) * updates;

        interval = ((float)max / updates);
    }

    /// blanks the current line by filling it with spaces
    void blank_line(std::ostream& o = std::cerr) const
    {
        o << "\r";
        for (int i = 0;
             i < cls + PSD + lbracketSize + rbracketSize + messageSize;
             ++i) {
            o << " ";
        }
        o << "\r";
    }

private: //     PRIVATE FIELDS
    step max;
    step current;
#ifdef UNIXLIKE
    std::string message;  ///< status report
    CS          bar;      ///< progress bar glyph
    csymvec     pbar;     ///< progress bar
    CS          lbracket; ///< left bracket
    CS          rbracket; ///< right bracket
    CS          pre;      ///< "background" glyph; fills progress bar
#elif defined   _WIN32
    std::string message;  ///< status report
    std::string bar;      ///< progress bar glyph
    symvec      pbar;     ///< progress bar
    std::string lbracket; ///< left bracket
    std::string rbracket; ///< right bracket
    std::string pre;      ///< "background" glyph; fills progress bar
#else
#error "unkown platform"
#endif

    step     interval; ///< the number of function calls between updates
    unsigned updates;  ///< the total of updates to be made
    unsigned pres;     ///< number of background glyphs required to fill line
    int      cls;      ///< the number of columns available for the progress bar
    int      barsize;  ///< the size of a single bar literal
    int      lbracketSize; ///< size of left bracket literal
    int      rbracketSize; ///< size of right bracket literal
    int      messageSize;  ///< size of message
};                         // Progressbar

} // LOAD

#endif // __LOAD__HPP
