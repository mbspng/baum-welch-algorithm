/**
 * @file filereader.hpp
 * Reads files.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __FILEREADER__
#define __FILEREADER__

#include <assert.h>
#include <boost/core/noncopyable.hpp>
#include <boost/io/ios_state.hpp>
#include <cstring>
#include <fstream>
#include <limits>

namespace aux {
class Filereader : private boost::noncopyable
{
public:
    class output_iterator
    {

    public:
        output_iterator(std::fstream&      file_m,
                        unsigned           line_number,
                        const std::string& file_name)
          : file_m(file_m), line_number(line_number), file_name(file_name)
        {
            file_m.seekg(0, std::ios::beg);
            go_to_line(0, line_number);
            if (std::getline(file_m, line)) {
                line_number++;
            } else {
                line_number = -1;
            }
        }

        output_iterator(std::fstream&      file_m,
                        int                line_number,
                        const std::string& file_name)
          : file_m(file_m), line_number(line_number), file_name(file_name)
        {
            if (line_number != -1) {
                throw std::invalid_argument("line number " +
                                            std::to_string(line_number) +
                                            " invalid. Permissible values are "
                                            "-1 or any unsigned integer.");
            }
        }

        const std::string& operator*() const { return line; }

        output_iterator& operator+=(size_t n)
        {
            for (size_t i = 0; i < n; ++i) {
                ++(*this);
            }
            return *this;
        }

        output_iterator& operator++()
        {
            if (std::getline(file_m, line)) {
                line_number++;
            } else {
                assert_not_bad();
                line_number = -1;
            }
            return *this;
        }

        bool is_empty() { return line == ""; }

        friend bool operator!=(const output_iterator& lhs,
                               const output_iterator& rhs)
        {
            return !(lhs == rhs);
        }

        friend bool operator==(const output_iterator& lhs,
                               const output_iterator& rhs)
        {
            if (&lhs.file_m != &rhs.file_m)
                throw(std::invalid_argument("file operands do not match"));
            return lhs.line_number == rhs.line_number;
        }

        friend bool operator==(const output_iterator& lhs,
                               const std::string&     rhs)
        {
            return lhs.line == rhs;
        }

    private:
        inline void assert_not_bad() const
        {
            if (file_m.bad()) {
                fprintf(stderr,
                        "Error while reading file %s: error code %d "
                        "('%s')\n",
                        file_name.c_str(),
                        errno,
                        std::strerror(errno));
                exit(1);
            }
        }

        inline void assert_not_fail() const
        {
            if (file_m.fail()) {
                throw std::runtime_error(
                  ("exception reading file '" + file_name + "'").c_str());
            }
        }

        inline void go_to_line(size_t from, size_t to)
        {
            assert(static_cast<int>(from) == line_number);

            if (from > to) {
                throw std::invalid_argument(
                  "from-line needs to come before to-line");
            }
            for (size_t i = from; i < to; ++i) {
                file_m.ignore(std::numeric_limits<std::streamsize>::max(),
                              '\n');
            }
        }

    private:
        std::fstream&      file_m;
        std::string        line;
        int                line_number;
        const std::string& file_name;
    }; // output_iterator

private:
    std::fstream      file_m;
    output_iterator   end_m;
    const std::string file_name;

public:
    Filereader(const std::string& file_name)
      : end_m(output_iterator(file_m, -1, file_name)), file_name(file_name)
    {
        load_file(file_name);
    }

    void close_file() { file_m.close(); }

    void load_file(const std::string& file_name)
    {
        file_m.open(file_name, std::ios::in);
        if (!file_m.is_open()) {
            fprintf(stderr,
                    "Opening file %s failed: error code %d ('%s')\n",
                    file_name.c_str(),
                    errno,
                    std::strerror(errno));
            exit(1);
        } else if (!file_m) {
            throw std::runtime_error("exception opening file '" + file_name +
                                     "'");
        }
        std::string line;
        if (std::getline(file_m, line)) {
            file_m.seekg(0);
        } else if (file_m.bad()) {
            fprintf(stderr,
                    "Error while reading file %s: error code %d "
                    "('%s')\n",
                    file_name.c_str(),
                    errno,
                    std::strerror(errno));
            exit(1);
        } else if (file_m.fail()) {
            fprintf(
              stderr,
              "Error while reading file %s (possible cause: file is empty)\n",
              file_name.c_str());
            exit(1);
        }
    }

    output_iterator begin()
    {
        return output_iterator(file_m, static_cast<unsigned>(0), file_name);
    }
    inline output_iterator end() { return end_m; }

    size_t get_line_count()
    {
        size_t                   line_count = 0;
        boost::io::ios_all_saver ias(file_m);
        for (auto line_itr = this->begin(); line_itr != this->end();
             ++line_itr) {
            line_count++;
        }
        ias.restore();
        return line_count;
    }
};
}
#endif
