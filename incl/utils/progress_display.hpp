/**
 * @file progress_display.hpp
 * Shows progress in form of a percantage.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef PROGRESS_DISPLAY
#define PROGRESS_DISPLAY

#include <iomanip>
#include <iostream>

#include "print_utils.hpp"

namespace aux {

struct ProgressDisplay
{

    ProgressDisplay(const size_t max, const std::string message, std::ostream& o = std::cerr)
      : max(max), message(message)
    {
        blank_line();
// do away with the cursor
#ifdef UNIXLIKE
        o << "\e[?25l";
#endif
    }

    void show(std::ostream& o = std::cerr)
    {
        o << message << std::setw(5) << std::left << ": "
                  << ((100 * ((double)current++ / max))) << "% \r";
//         o.flush();
        if (current == max) {
            cancel();
            current = 0;
        }
    }

    void run(std::ostream& o = std::cerr){
        show(o);
    }

    /// blanks the current line and brings back the cursor
    void cancel(std::ostream& o = std::cerr) const
    {
        blank_line();
#ifdef UNIXLIKE
        o << "\e[?25h";
#endif
    }

    /// blanks the current line by filling it with spaces
    void blank_line(std::ostream& o = std::cerr) const
    {
        unsigned cls = aux::get_terminal_columns();
        o << "\r";
        for (unsigned i = 0; i < cls; ++i) {
            o << " ";
        }
        o << "\r";
    }

    long        current{ 0 };
    long        max;
    std::string message;
};
}

#endif
