//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __AUX_PRINT__HPP
#define __AUX_PRINT__HPP

#if defined __linux || defined __APPLE__
#define UNIXLIKE
#endif

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#ifdef UNIXLIKE
#include <sys/ioctl.h>
#include <unistd.h>
#ifdef __ANDROID__
#include <termios.h>
#endif
#endif
#ifdef _WIN32
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#define NOMINMAX
#endif
#include <io.h>
#include <windows.h>
#endif

namespace aux {
/// returns number of columns of current CLI window (terminal or cmd)
unsigned
get_terminal_columns()
{
    unsigned c = 0;
#ifdef _WIN32
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    c = csbi.srWindow.Right - csbi.srWindow.Left;
#elif defined UNIXLIKE
    struct winsize w;
    // ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    ioctl(0, TIOCGWINSZ, &w);
    c = w.ws_col;
#else
#error "Unkown platform"
#endif
    return c;
}
} // aux




#endif
