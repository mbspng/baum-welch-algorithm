/**
 * @file busy.hpp
 * Classes for a bar that indicates a running process
 *
 * compilers:
 *            - clang-700.0.72
 *            - clang 3.5.2 / 3.5.0
 *            - GCC 5.2.0 / 4.8.3
 *            - Microsoft C/C++ 19.00.23026 for x86
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __BUSY__HPP
#define __BUSY__HPP

#include <iostream>

#ifndef UNIXLIKE
#if defined __linux || defined __APPLE__
#define UNIXLIKE
#endif
#endif

#include "color_utils.hpp"
#include "print_utils.hpp"
#include "string_utils.hpp"
#ifdef UNIXLIKE
#include <unistd.h>
#endif

namespace aux {

//                                  INCREMENT                                 //

/**
     * @brief handle for variable to pass to generic functions for controlling
     * the functionn from the outside. If the function takes an argument that it
     * applies operator++ to, this handle redirects the ++ operation to its
     * own member function.
     */
template<typename T>
class Increment
{

public:
    Increment(T& v, T i = 1)
    {
        increment = i;
        value     = &v;
    }

    /// adds whatever has been set as the increment to the variable the
    /// value-pointer has been set to
    T operator++()
    {
        *value = *value + (increment);
        return *value;
    }

    T* value;
    T  increment;
};

typedef auxCOLOR::ColorString CS;

class BusyBlinker
{
public:
    BusyBlinker(unsigned          pace,
                std::string       message = "",
                auxCOLOR::ColorID s1c     = auxCOLOR::ColorID::RED,
                auxCOLOR::ColorID s2c     = auxCOLOR::ColorID::RED,
                std::string s1 = " ◉ ",
                std::string s2 = " ◯ ")
      :
#ifdef UNIXLIKE
      s1(s1, s1c)
      , s2(s2, s2c)
      ,
#else
      s1("d")
      , s2("p")
      ,
#endif
      message(message + " ")
      , s1Size(aux::utf8_size(s1))
      , s2Size(aux::utf8_size(s2))
      , messageSize(aux::utf8_size(message) / s1Size)
      , off(false)
    {
        init(pace);
    }

    /// updates the indicator bar
    inline void run(std::ostream& o = std::cerr)
    {
// do not display anything, if stdout is redirected to a file
#ifdef _WIN32
        if (!_isatty(_fileno(stdout))) return;
#else
        if (!isatty(fileno(stdout))) return;
#endif
        // if an interval is over, update
        if (tick == interval) {
#ifdef UNIXLIKE
            o << "\e[?25l";
#endif
            if (off) {
                o << message << s1 << '\r';
                off = false;
            } else {
                o << message << s2 << '\r';
                off = true;
            }
            tick = 0;
        }
        ++tick;
    }

private: //   PRIVATE METHODS
    /// initializes fields
    void init(unsigned pace)
    {
        // query the terminal column count
        cls                = aux::get_terminal_columns();
        if (cls == -1) cls = 30;

        // normalise speed on window width (column number)
        interval = pace * ((double)s1Size / cls);
        // interval/((double)s1Size/cls) same value in all terminals

        tick = interval;
    }

protected: //  PROTECTED FIELDS
#ifdef _WIN32
    const std::string s1; ///< bar element
    const std::string s2; ///< bar element
#else
    const CS s1; ///< bar element
    const CS s2; ///< bar element
#endif

    const std::string message; ///< status report
    int               s1Size;  ///< size of the \b s1 literal
    int               s2Size;  ///< size of the \b s2 literal
    int               messageSize;

    int                cls;      ///< number of columns
    int                capacity; ///< line capacity for specifc string
    unsigned long      tick;     ///< calls since last display update
    unsigned long long interval; ///< update interval in function calls
    bool               off;
};

//                                                                            //
//                                  BusyBar                                   //
//                                                                            //

/**
 * @brief can display a a bar indicating a busy process the CLI window
 */
class BusyBar
{

public: //    PUBLIC METHODS
    /**
     * @brief sets the glyphs for the different parts of the indicator bar
     * @param pace blinking frequency
     * @param message message to display while blinking
     * @param s1c color for @p s1
     * @param s2c color for @p s2
     * @param s3c color for @p s3
     * @param str1 part of the indicator bar (specifics depend on derived class)
     * @param str2 part of the indicator bar (specifics depend on derived class)
     * @param str3 part of the indicator bar (specifics depend on derived class)
     */
    BusyBar(unsigned          pace,
            std::string       message,
            auxCOLOR::ColorID s1c,
            auxCOLOR::ColorID s2c,
            auxCOLOR::ColorID s3c,
            std::string       str1,
            std::string       str2,
            std::string       str3)
      :
#ifdef UNIXLIKE
      s1(str1, s1c)
      , s2(str2, s2c)
      , s3(str3, s3c)
      ,
#else
      s1("#")
      , s2("#")
      , s3("-")
      ,
#endif
      message(message + " ")
      , s1Size(aux::utf8_size(s1))
      , s2Size(aux::utf8_size(s2))
      , s3Size(aux::utf8_size(s3))

      , messageSize(aux::utf8_size(message) / s1Size)
      , occupied(0)
      , ocup(occupied, 1)
      , ocdown(occupied, -1)
    {
        init(pace);
    }

    /// resets the line the indicator bar was running on
    void cancel(std::ostream& o = std::cerr)
    {
// do not display anything, if stdout is redirected to a file
#ifdef _WIN32
        if (!_isatty(_fileno(stdout))) return;
#else
        if (!isatty(fileno(stdout))) return;
#endif
        blank_line(o);
#ifdef UNIXLIKE
        o << "\e[?25h";
#endif
    }

    /// updates the indicator bar
    template<typename T>
    inline void run(const T&      cf,
                    const T&      cb,
                    const T&      tfl,
                    const T&      tfr,
                    const T&      tbl,
                    const T&      tbr,
                    std::ostream& o = std::cerr)
    {
// do not display anything, if stdout is redirected to a file
#ifdef _WIN32
        if (!_isatty(_fileno(stdout))) return;
#else
        if (!isatty(fileno(stdout))) return;
#endif
        // if an interval is over, update
        if (tick == interval) {
#ifdef UNIXLIKE
            o << "\e[?25l";
#endif
            // if moving right
            if (right) {
                // test whether the is still capacity left for 1 more update
                if (capacity - occupied >= 1) {
                    // if so, update
                    next(tfl, cf, tfr, ocup);
                }
                // if not blank the line and reverse the direction
                else {
                    blank_line(o);
                    right = false;
                }
            }
            // if moving left
            if (!right) {
                // test whether there is still capacity left for 1 more update
                if (occupied - 1 >= 0) {
                    // if so, update
                    next(tbl, cb, tbr, ocdown);
                }
                // if not blank the line and reverse the direction
                else {
                    blank_line(o);
                    right = true;
                    next(tfl, cf, tfr, ocup);
                }
            }
            tick = 0;
        }
        ++tick;
    }

private: //   PRIVATE METHODS
    /// initializes fields
    void init(unsigned pace)
    {
        // query the terminal column count
        cls                = aux::get_terminal_columns();
        if (cls == -1) cls = 30;

        // normalise speed on window width (column number)
        interval = pace * ((double)s3Size / cls);
        // interval/((double)s3Size/cls) same value in all terminals

        right = true;
        tick  = 0;
    }

protected: // PROTECTED METHODS
    /**
     * @brief calculates the number of @p js that fit on one line together with
     *        one @p i. Returns false if less than 1 j fits on the line.
     * @param j some literal
     * @param i soem literal
     */
    template<typename T>
    bool calc_cap(T i, T j)
    {
        capacity = ((int)(cls - i) / j) - messageSize;
        if (capacity == 0) return false;
        return true;
    }

    /**
     * @brief fills line incrementally with pattern s1* s2 s3*. Per call the
     *        line is updated, advancing the position of all moving elements
     *        of the indicator bar.
     * @tparam T the type of the literal arguments
     * @param s1 some literal
     * @param s2 some literal
     * @param s3 some literal
     * @param incr \b Increment functor
     * @param o stream to send to
     */
    template<typename T>
    void next(const T&        s1,
              const T&        s2,
              const T&        s3,
              Increment<int>& incr,
              std::ostream&   o = std::cerr)
    {
        o << message;
        // send left part of bar
        for (int i = 0; i < occupied; ++i) {
            o << s1;
        }
        // send head
        o << s2;
        // send right part of bar
        for (int i = occupied; i < capacity; ++i) {
            o << s3;
        }
        o << "\r";
        o.flush();
        // increment/ decrement \b occupied through the interface of the functor
        // @p incr. Depending on which functor was passed, occupied will be
        // incremented or decremented.
        ++incr;
    }

    /**
     * @brief fills line with literal @p sym
     * @tparam T type of the literal
     * @param sym some literal
     * @pre @p needs to have string conversion defined
     * @param o stream to send to
     */
    template<typename T>
    void fill_line(T& sym, std::ostream& o = std::cerr) const
    {
        o << "\r";
        // get the number of glyphs in @p sym and fill the line accordingly
        // (getting the number of chars rather than glyphs  would potentially
        // spill over into the next line)
        for (int i = 0; i < cls / aux::utf8_size(sym); ++i) {
            o << sym;
        }
        o << "\r";
        o.flush();
    }

    /**
     * @brief fills line with whitespace
     * @param o stream to send to
     */
    void blank_line(std::ostream& o = std::cerr) const
    {
        o << "\r";
        for (int i = 0; i < cls; ++i) {
            o << " ";
        }
        o << "\r";
        o.flush();
    }

protected: //  PROTECTED FIELDS
#ifdef _WIN32
    const std::string s1; ///< bar element
    const std::string s2; ///< bar element
    const std::string s3; ///< bar element
#else
    const CS s1; ///< bar element
    const CS s2; ///< bar element
    const CS s3; ///< bar element
#endif

    const std::string message; ///< status report
    int               s1Size;  ///< size of the \b s1 literal
    int               s2Size;  ///< size of the \b s2 literal
    int               s3Size;  ///< size of the \b s3 literal
    int               messageSize;

    int                cls;      ///< number of columns
    int                capacity; ///< line capacity for specifc string
    int                occupied; ///< used up capacity counting from the left
    unsigned long      tick;     ///< calls since last display update
    unsigned long long interval; ///< update interval in function calls
    bool               right;

    Increment<int> ocup;   ///< handle for occupied variable; increments
    Increment<int> ocdown; ///< handle for occupied variable; decrements
};

//                                                                            //
//                                   BusyBar1                                 //
//                                                                            //

/**
 * @brief Variant with a changing moving head literal and a filler literal
 *        the first version of the head moves left, the other right. The
 *        filler covers the rest of the line
 */
class BusyBar1 : public BusyBar
{

public: //   PUBCLIC METHODS
    /**
     * @brief sets the glyphs for the different parts of the indicator bar
     * @param pace blinking frequency
     * @param message message to display while blinking
     * @param s1c color for @p s1
     * @param s2c color for @p s2
     * @param s3c color for @p s3
     * @param s1 left moving head
     * @param s2 right moving head
     * @param s3 line filler
     */
    BusyBar1(unsigned          pace,
             std::string       message = "",
             auxCOLOR::ColorID s1c     = auxCOLOR::ColorID::RED,
             auxCOLOR::ColorID s2c     = auxCOLOR::ColorID::GREEN,
             auxCOLOR::ColorID s3c     = auxCOLOR::ColorID::BLACK,
             std::string s1 = " ◀ ",
             std::string s2 = " ▶ ",
             std::string s3 = " ● ")
      : BusyBar(pace, message, s1c, s2c, s3c, s1, s2, s3)
    {
        init();
    }

    inline virtual void run(std::ostream& o = std::cerr)
    {
        BusyBar::run(s2, s1, s3, s3, s3, s3);
    }

private: //   PRIVATE METHODS
    /// initializes fields
    void init()
    {
        // s1 and s2 need to be same size, because they substitute each
        // other depening on the heading
        if (s1Size != s2Size) {
            throw std::invalid_argument(
              "paired symbols s1 and s2 require same number of glyphs.");
        }

        // calculate the number of s3s that fit in a line together
        // with an s1 respectively s2, as there is  only ever one s1
        // resp. s2 on the line
        if (!calc_cap(s1Size, s3Size)) {
            throw std::invalid_argument(
              "s1 and s3 combined exceed line capacity.");
        }
    }
};

//                                                                            //
//                                   BusyBar2                                 //
//                                                                            //

/**
 *@brief Variant with one unchanging moving head, a left filler and a right
 *       filler. The head moves over the lines back and forth, the fillers
 *       cover either side of it.
 */
class BusyBar2 : public BusyBar
{

public: //    PUBLIC METHODS
    /**
     * @brief sets the glyphs for the different parts of the indicator bar
     * @param pace blinking frequency
     * @param message message to display while blinking
     * @param s1c color for @p s1
     * @param s2c color for @p s2
     * @param s3c color for @p s3
     * @param s1 left filler
     * @param s2 moving head
     * @param s3 right filler
     */
    BusyBar2(unsigned          pace,
             std::string       message = "",
             auxCOLOR::ColorID s1c     = auxCOLOR::ColorID::GREEN,
             auxCOLOR::ColorID s2c     = auxCOLOR::ColorID::RED,
             auxCOLOR::ColorID s3c     = auxCOLOR::ColorID::GREEN,
             std::string s1 = " ◉ ",
             std::string s2 = " ◉ ",
             std::string s3 = " ◯ ")
      : BusyBar(pace, message, s1c, s2c, s3c, s1, s2, s3)
    {
        init();
    }

    inline virtual void run(std::ostream& o = std::cerr)
    {
        BusyBar::run(s2, s2, s1, s3, s1, s3);
    }

private: //   PRIVATE METHODS
    /// initialises fields
    void init()
    {
        // s1 and s3 need to be same size, because they substitute each
        // other depening on the heading
        if (s1Size != s3Size) {
            throw std::invalid_argument(
              "paired symbols s1 and s2 require same number of glyphs.");
        }

        // calculate the number of s1s that fit in a line together
        // with an s1 respectively s3, as there is  only ever one
        // s2 on the line
        if (!calc_cap(s2Size, s1Size)) {
            throw std::invalid_argument(
              "s1 and s3 combined exceed line capacity.");
        }
    }
};

} // BUSY

#endif // __BUSY__HPP
