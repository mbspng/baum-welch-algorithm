/**
 * @file combinator.hpp
 * Combines two containers to tuples in a lazy fashion.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef TOKEN__COMBINATOR
#define TOKEN__COMBINATOR

#include <iostream>
#include <vector>

template<class tuple_type>
struct tuple_1st_projection_access_policy
{
    using value_type = typename tuple_type::first_type;
    const auto& access(const tuple_type& tuple) { return tuple.first; }
};

template<class T>
struct identity_access_policy
{
    using value_type = T;
    const auto& access(const T& t) { return t; }
};

template<class pool_type,
         template<class> class access_policy_type = identity_access_policy>
struct Combinator : public access_policy_type<typename pool_type::value_type>
{
    using access_policy_type<typename pool_type::value_type>::access;
    // Remove const as type of element might be const, leading to
    // combianto field cells being impossible to be asigned new values
    using element_type = typename std::remove_const<typename access_policy_type<
      typename pool_type::value_type>::value_type>::type;
    using combination_type = std::vector<element_type>;

    Combinator(const pool_type& pool, int length)
      : pool(pool)
      , length(length)
      , combination(length)
      , iterators(length, pool.begin())
    {
        if (length == 0)
            throw std::invalid_argument("combination of length 0 is undefined");
        auto j = length;
        while (j--) {
            combination[j] = access(*(pool.begin()));
        }
    }

    const combination_type& make_combination()
    {
        make_combination(0);
        return combination;
    }

    combination_type get_combination() { return combination; }

private:
    bool make_combination(int current_length)
    {

        auto& i = iterators[current_length];
        if (current_length == length - 1) {

            if (i == pool.end()) {
                i                           = pool.begin();
                combination[current_length] = access(*i);
                (i = pool.begin())++;
                return true;
            }
            combination[current_length] = access(*i);
            i++;
            return false;
        }

        if (make_combination(current_length + 1)) {

            if (++i == pool.end()) {
                --i;
                i                           = pool.begin();
                combination[current_length] = access(*i);
                return true;
            }
            combination[current_length] = access(*i);
            return false;
        }
        combination[current_length] = access(*i);
        return false;
    }

private:
    const pool_type&                                pool;
    int                                             length;
    combination_type                                combination;
    std::vector<typename pool_type::const_iterator> iterators;
};

#endif
