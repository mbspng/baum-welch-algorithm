/**
 * @file variant_translator.hpp
 * Translator emulates a bi-directional mapping from external to
 * internal IDs.
 *
 * compilers:
 *            - Apple LLVM version 8.1.0 (clang-802.0.42)
 *            - clang version 3.8.0-2ubuntu4
 *            - g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
 */

//MIT License
//
//Copyright (c) 2016 Matthias Bisping
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

#ifndef __VARIANT_TRANSLATOR__
#define __VARIANT_TRANSLATOR__

#include <boost/functional/hash.hpp>
#include <boost/variant.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

namespace aux {

template<class V, class T>
class TranslatorSpecialization
{
public:
    using ext_sym_type = V;
    using int_sym_type = size_t;
    using obj2id_type =
      std::unordered_map<ext_sym_type, int_sym_type, boost::hash<ext_sym_type>>;
    using value_type  = typename obj2id_type::const_iterator;
    using id2obj_type = std::vector<value_type>;

protected:
    /**
     *  @brief translates an external tokens to an internal ID
     *  @param es the external token to translate into an internal ID
     *  @return the internal ID assigned to the external token
     */
    const int_sym_type& get(const T& es, std::false_type)
    {
        auto s = obj2id.find(es);
        if (s == obj2id.end()) {
            id2obj_type*       id2obj_ref = get_id2obj();
            const int_sym_type is         = id2obj_ref->size();
            s                             = obj2id.begin();
            s = obj2id.insert(s, std::make_pair(es, is));
            id2obj_ref->push_back(s);
            return s->second;
        }
        return s->second;
    }
    virtual id2obj_type* get_id2obj() = 0;

private:
    // every inherited specialized subclass has its own obj2id map
    obj2id_type obj2id;
};

template<class V, class...>
class TranslatorSpecializationDriver;

template<class V>
class TranslatorSpecializationDriver<V>
{
public:
    struct dummy
    {
    };
    void get(dummy&, dummy&);
    void get_id2obj();
};

template<class V, class T, class... Args>
class TranslatorSpecializationDriver<V, T, Args...>
  : protected TranslatorSpecializationDriver<V, Args...>
  , protected TranslatorSpecialization<V, T>
{
public:
    using TranslatorSpecializationDriver<V, Args...>::get;
    using TranslatorSpecialization<V, T>::get;
    using TranslatorSpecializationDriver<V, Args...>::get_id2obj;
    using TranslatorSpecialization<V, T>::get_id2obj;
    using typename TranslatorSpecialization<V, T>::value_type;
    using typename TranslatorSpecialization<V, T>::id2obj_type;
};

// maps variants of any non-integral types to size_t
// the class is constructed via recursive and multiple inheritance like so:
// Translator<T1, T2, T3>
// ↑
// TranslatorSpecializationDriver<(T1, T2, T3), T1, T2, T3> ,
// TranslatorSpecialization<T1> ↑ TranslatorSpecializationDriver<(T1, T2, T3),
// T3> , TranslatorSpecialization<T2> ↑ TranslatorSpecializationDriver<(T1, T2,
// T3)> , TranslatorSpecialization<T3>
template<class T, class... Args>
class Translator
  : protected TranslatorSpecializationDriver<boost::variant<T, size_t, Args...>,
                                             T,
                                             Args...>
{
public:
    using int_sym_type = size_t;
    using ext_sym_type = boost::variant<T, int_sym_type, Args...>;
    using TranslatorSpecializationDriver<ext_sym_type, T, Args...>::get;
    using typename TranslatorSpecializationDriver<ext_sym_type, T, Args...>::
      value_type;
    using typename TranslatorSpecializationDriver<ext_sym_type, T, Args...>::
      id2obj_type;
    /**
     * @brief operator[] deligates to specialized private get()-functions,
     * integral types will be interpreted as IDs all other types as external
     * symbols
     */
    template<class U>
    const ext_sym_type operator[](const U& is)
    {
        try {
            return get(is, std::is_integral<U>());
        } catch (std::domain_error& e) {
            throw e;
        } catch (...) {
            std::stringstream ss;
            ss << is;
            throw std::invalid_argument("access with " + ss.str() + " failed");
        }
    }

private:
    // id2obj is a vector of iterators into the individual obj2id maps
    // of the base classes
    id2obj_type id2obj;

private:
    id2obj_type* get_id2obj() { return &id2obj; }
    /**
     *  @brief translates an internal ID to an external token @param
     *  is the internal ID to translate to an external token
     *  @return the external token associated with the internal ID
     */
    const ext_sym_type& get(const int_sym_type& is, std::true_type)
    {
        if (is < id2obj.size()) {
            return (id2obj[is])->first;
        }
        throw std::domain_error("unknown ID: " + std::to_string(is));
    }
};
}

#endif
