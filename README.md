## An Implementation of the Baum-Welch Algorithm

This repository contains a C++ implementation of the Baum-Welch Algorithm, which is an expectation maximization variant for Hidden Markov Models.

### Requirements

* C++ compiler; tested with:
    * Apple LLVM version 8.1.0 (clang-802.0.42)
    * clang version 3.8.0-2ubuntu4
    * g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
    * c++ (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0
* Python 3.8 or higher

### Setup (Linux / apt)

```bash
apt-get install libboost-all-dev
apt-get install cmake
# optionally for documentation  generation
apt-get install doxygen
./setup.sh
```

### Usage

For convenience data to train a model is included in an archive and unpacked by setup.sh to `data`. The following demonstrates
how a model can be first trained on a labeled training set via maximum likelihood estimate and then continued to be trained
on an unlabeled dataset via expectation maximization.

1) Bootstrap a model with labeled data.
  ```bash
  # MLE training
  $ src/train.py supervised data/tiger_10prct/tiger_annotated_10prct > /tmp/bootstrap_model.txt
  ```
2) Train the model further with unlabeled data. 
```bash
# EM training
$ src/train.py unsupervised data/tiger_90prct/tiger_unannotated_90prct warm /tmp/bootstrap_model.txt > /tmp/final_model.txt
```
Now the model can be used to label new data.
```bash
# prediction
$ src/analyze.py /tmp/final_model.txt data/tiger_90-100prct/tiger_unannotated_90-100prc
```

### License

License applies on a per-file basis. Specifically the tiger corpus is not commercially free: https://www.ims.uni-stuttgart.de/documents/ressourcen/korpora/tiger-corpus/license/htmlicense.html

